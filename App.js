/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {Provider} from 'react-redux';
import Store from './src/redux/store';
import Navigator from './src/navigation/navigation';
import MapBoxGL from '@react-native-mapbox-gl/maps';
import './src/Reactotronconfig';
MapBoxGL.setAccessToken(
  'pk.eyJ1IjoiYnJpYW4tbW91cmEiLCJhIjoiY2swOGpwcWM5MGFnODNtbHRtZWtheDF2cSJ9.5oApmoKX2k_gvJ9Bj4AkZQ',
);


const App = () => {
  return (
    <Provider store={Store}>
      <Navigator />
    </Provider>
  );
};

export default App;
