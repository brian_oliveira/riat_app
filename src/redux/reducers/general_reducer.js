import Actions from '../actions/actions';

const initialState = {
  stitchClient: null,
};

const GeneralReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.setStitchClient:
      return {...state, stitchClient: action.payload};
    default:
      return state;
  }
};

export default GeneralReducer;
