import Actions from '../actions/actions';

const initialState = {
  generalData: {},
  paciente: {},
  instituicao: {},
  profissionalPreferencias: {},
  profissionalExperiencia: {},
  tiposProfissional: [],
};

const RegisterReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.setGeneral:
      return {...state, generalData: action.payload};
    case Actions.setPaciente:
      return {...state, paciente: action.payload};
    case Actions.setInstituicao:
      return {...state, instituicao: action.payload};
    case Actions.setProfissionalPreferencias:
      return {...state, profissionalPreferencias: action.payload};
    case Actions.setProfissionalExperiencia:
      return {...state, profissionalExperiencia: action.payload};
    case Actions.setProfissionalTypes:
      return {...state, tiposProfissional: action.payload};
    default:
      return state;
  }
};

export default RegisterReducer;
