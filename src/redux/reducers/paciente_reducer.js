import Actions from '../actions/actions';

const initialState = {
  pacientes: [],
  pacienteSelected: {},
};

const PacienteReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.setPacienteGeral:
      return {...state, pacienteSelected: action.payload};
    case Actions.setPacientesGeral:
      return {...state, pacientes: action.payload};
    default:
      return state;
  }
};

export default PacienteReducer;
