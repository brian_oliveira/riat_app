import Actions from '../actions/actions';

const initialState = {
  familiar: {},
  pacientes: [],
  solicitaciones: {},
  pacienteSelected: {},
};

const FamiliarReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.getPacientesSuccess:
      return {...state, pacientes: action.payload};
    case Actions.getSolicitacoes:
      return {...state, solicitaciones: action.payload};
    case Actions.selectPacienteFamiliar:
      return {...state, pacienteSelected: action.payload};
    default:
      return state;
  }
};

export default FamiliarReducer;
