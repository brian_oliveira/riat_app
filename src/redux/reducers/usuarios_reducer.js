import Actions from '../actions/actions';

const initialState = {
  userLogged: {},
  loggging: false,
};

const UsuariosReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.getUsuarios:
      return {...state, usuarios: action.payload};
    case Actions.loggingIn:
      return {...state, logging: true};
    case Actions.logged:
      return {...state, userLogged: action.payload, logging: false};
    case Actions.loggedFail:
      return {...state, logging: false};
    default:
      return state;
  }
};

export default UsuariosReducer;
