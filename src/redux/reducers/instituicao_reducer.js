import Actions from '../actions/actions';

const initialState = {
  pacientes: [],
  pacienteSelected: {},
  instituicao: {},
  medicos: [],
  sessoes: [],
};

const InstituicaoReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.getPacientesInstituicao:
      return {...state, pacientes: action.payload};
    case Actions.selectPacienteInstituicao:
      return {...state, pacienteSelected: action.payload};
    case Actions.setMedicos:
      return {...state, medicos: action.payload};
    case Actions.setSessoesInstituicao:
      return {...state, sessoes: action.payload};
    default:
      return state;
  }
};

export default InstituicaoReducer;
