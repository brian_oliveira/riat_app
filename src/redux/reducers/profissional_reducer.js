import Actions from '../actions/actions';

const initialState = {
  selectedSolicitacao: {},
  sessoes: [],
  sessaoSelected: {},
};

const ProfissionalReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.selectSolicitacao:
      return {...state, selectedSolicitacao: action.payload};
    case Actions.setSessoesFromAcompanhante:
      return {...state, sessoes: action.payload};
    case Actions.selectSessao:
      return {...state, sessaoSelected: action.payload};
    default:
      return state;
  }
};

export default ProfissionalReducer;
