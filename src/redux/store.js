import {createStore, combineReducers, applyMiddleware} from 'redux';
import UsuariosReducer from './reducers/usuarios_reducer';
import thunk from 'redux-thunk';
import RegisterReducer from './reducers/register_reducer';
import FamiliarReducer from './reducers/familiar_reducer';
import InstituicaoReducer from './reducers/instituicao_reducer';
import GeneralReducer from './reducers/general_reducer';
import ProfissionalReducer from './reducers/profissional_reducer';
import PacienteReducer from './reducers/paciente_reducer';

const Store = createStore(
  combineReducers({
    general: GeneralReducer,
    usuarios: UsuariosReducer,
    profissional: ProfissionalReducer,
    register: RegisterReducer,
    familiar: FamiliarReducer,
    instituicao: InstituicaoReducer,
    paciente: PacienteReducer,
  }),
  applyMiddleware(thunk),
);

export default Store;
