import {UserPasswordCredential} from 'mongodb-stitch-core-sdk';
import axios from 'axios';
import {URL} from '../../assets/consts';

const updateFromServer = (id, email, callback) => {
  axios
    .post(URL + 'usuarios/incoming_webhook/loginUserCheck', {
      userId: id,
      email: email,
    })
    .then(callback)
    .catch(err => alert(err));
};

const performLoginOnServer = (props, credential) => {
  props.stitchClient.auth
    .loginWithCredential(credential)
    .then(user => {
      updateFromServer({$oid: user.id}, credential.username, res => {
        props.logged(res.data);
        if (res.data.hasOwnProperty('userType')) {
          switch (res.data.userType) {
            case 'familiar':
              props.navigation.navigate('FamiliarMenu');
              break;
            case 'profissional':
              props.navigation.navigate('ProfissionalMenu');
              break;
            case 'funcionario':
              props.navigation.navigate('InstituicaoMenu');
              break;
            case 'psi':
              props.navigation.navigate('PsiMenu');
          }
        }
      });
    })
    .catch(err => {
      props.loggedFail();
      alert('E-mail o contraseña invalidos.');
    });
};

const doMockLogin = async (props, type, email, password) => {
  props.loggingIn();
  performLoginOnServer(props, new UserPasswordCredential(email, password));
};

export {doMockLogin, performLoginOnServer, updateFromServer};
