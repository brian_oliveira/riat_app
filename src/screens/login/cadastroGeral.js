import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  TextInput,
  Text,
  Alert,
  Dimensions,
} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import axios from 'axios';
import {URL, IMAGE_PICKER_BASE_CONFIG, optionsS3} from '../../assets/consts';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from 'react-native-image-picker';
import {
  Stitch,
  UserPasswordAuthProviderClient,
} from 'mongodb-stitch-react-native-sdk';
import {RNS3} from 'react-native-s3-upload/src/RNS3';

const radioGenero = [
  {label: 'Masculino', value: 'masculino'},
  {label: 'Femenino', value: 'femenino'},
  {label: 'Otro', value: 'otro'},
];

const radioType = [
  {label: 'Familiar', value: 'familiar'},
  {label: 'Profissional', value: 'profissional'},
  {label: 'Institucional', value: 'institucional'},
];

const sendDataToServer = async (data, callbackSuccess = () => {}) => {
  try {
    //Cria factory de criação de Usuários
    const emailPasswordClient = Stitch.defaultAppClient.auth.getProviderClient(
      UserPasswordAuthProviderClient.factory,
    );
    //Faz requisições para a adição de Usuários no Stitch e no banco.
    await axios.post(URL + 'familiar/incoming_webhook/createFamiliar', data);
    await emailPasswordClient.registerWithEmail(
      data.usuario.email,
      data.usuario.password,
    );
  } catch (err) {
    Alert.alert('Perdón!', 'Aconteció un error... ' + err);
  } finally {
    Alert.alert(
      'Sucesso!',
      'Bienvenido a RIAT! Por favor, verifique su email.',
    );
    callbackSuccess();
  }
};

const CadastroGeral = props => {
  const [registerData, setData] = useState({
    nombre: '',
    fechaNacimiento: '',
    direccion: '',
    genero: 'masculino',
    DNI: '',
    telefono: '',
    email: '',
    contrasena: '',
    type: 'familiar',
    imageUri: '',
    userprofile_image: '',
  });

  const [confirmContrasena, setConfirmContrasena] = useState('');

  const validateForm = form => {
    if (
      form.nombre.length > 0 &&
      form.fechaNacimiento.length > 0 &&
      form.direccion.length > 0 &&
      form.DNI.length > 0 &&
      form.telefono.length > 0 &&
      form.email.length > 0 &&
      form.contrasena.length > 0 &&
      form.imageUri.length > 0
    ) {
      console.log('form valido');
      return true;
    }
    console.log('form faltando dados');
    return false;
  };

  const [loading, setLoading] = useState(false);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity
        style={styles.imageContainer}
        onPress={async () => {
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              try {
                console.log(response);
                if (!response.didCancel) {
                  setData({
                    ...registerData,
                    imageUri: response.uri,
                    userprofile_image: response.data,
                  });
                }
              } catch (err) {
                alert('Erro! ' + err);
              }
            },
          );
        }}>
        {registerData.userprofile_image ? (
          <Image
            style={styles.image}
            source={{
              uri: registerData.imageUri,
            }}
          />
        ) : (
          <Icon name="camera" style={styles.icon} />
        )}
      </TouchableOpacity>
      <Text style={styles.textLabel}>Nombre Completo</Text>
      <TextInput
        onChangeText={text => setData({...registerData, nombre: text})}
        style={[styles.textInput]}
        placeholder={'Ejemplo: Juan Contreras'}
      />
      <Text style={[styles.textLabel, {marginBottom: 10}]}>Género</Text>
      <RadioForm
        radio_props={radioGenero}
        initial={0}
        onPress={value => setData({...registerData, genero: value})}
      />
      <Text style={styles.textLabel}>Fecha de Nacimiento</Text>
      <DatePicker
        style={{width: Dimensions.get('window').width - 30}}
        format={'DD/MM/YYYY'}
        customStyles={{
          dateInput: [styles.textInput, {paddingTop: 20, paddingBottom: 20}],
          dateIcon: {display: 'none'},
        }}
        onDateChange={date => setData({...registerData, fechaNacimiento: date})}
        date={registerData.fechaNacimiento}
        placeholder={'Ejemplo: 10/10/2010'}
      />
      <Text style={styles.textLabel}>Dirección</Text>
      <TextInput
        onChangeText={text => setData({...registerData, direccion: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: Calle 10, 32'}
      />
      <Text style={styles.textLabel}>DNI</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={text => setData({...registerData, DNI: text})}
        placeholder={'Ejemplo: 9999999999'}
      />
      <Text style={styles.textLabel}>Teléfono</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={text => setData({...registerData, telefono: text})}
        placeholder={'Ejemplo: 11 1234-489-2839'}
      />
      <Text style={styles.textLabel}>E-mail</Text>
      <TextInput
        onChangeText={text => setData({...registerData, email: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: alguien@provedor.com'}
      />
      <Text style={styles.textLabel}>Contraseña</Text>
      <TextInput
        onChangeText={text => setData({...registerData, contrasena: text})}
        style={styles.textInput}
        secureTextEntry={true}
      />
      <Text style={styles.textLabel}>Confirmar Contraseña</Text>
      <TextInput
        onChangeText={text => setConfirmContrasena(text)}
        style={styles.textInput}
        secureTextEntry={true}
      />
      <Text style={[styles.textLabel, {marginBottom: 10}]}>
        Regístrese como:
      </Text>
      <RadioForm
        radio_props={radioType}
        initial={0}
        onPress={value => {
          setData({...registerData, type: value});
        }}
      />
      <TouchableOpacity
        style={[styles.btnNext, loading ? styles.loading : {}]}
        disabled={loading}
        onPress={async () => {
          if (
            validateForm(registerData) &&
            confirmContrasena === registerData.contrasena
          ) {
            setLoading(true);
            props.setGeneral(registerData);
            switch (registerData.type) {
              case 'familiar':
                let data = {...registerData};
                let file = {
                  uri: data.imageUri,
                  name: 'profile_pic.jpg',
                  type: 'image/jpg',
                };

                RNS3.put(file, optionsS3).then(response => {
                  if (response.status !== 201) {
                    alert(
                      'Problema de conexión con la base de datos. Por favor, intente más tarde.',
                    );
                    setLoading(false);
                  }
                  delete data.imageUri;
                  data = {
                    ...data,
                    profile_url: response.body.postResponse.location,
                  };
                  sendDataToServer(
                    {
                      ...data,
                      usuario: {
                        email: registerData.email.toLowerCase(),
                        password: registerData.contrasena,
                        type: 'familiar',
                      },
                    },
                    () => {
                      setLoading(false);
                      props.navigation.navigate('Home');
                    },
                  );
                });
                break;
              case 'profissional':
                setLoading(false);
                props.navigation.navigate('TipoProfesional');
                break;
              case 'institucional':
                setLoading(false);
                props.navigation.navigate('CadastroInstituicao');
                break;
              default:
                setLoading(false);
                alert('Erro ao executar comando.');
            }
          } else if (confirmContrasena != registerData.contrasena) {
            Alert.alert(
              'Error',
              'Su confirmación de contraseña no es igual a su contraseña.',
            );
          } else if (registerData.contrasena.length < 6) {
            Alert.alert(
              'Error',
              'La contraseña debe tener más de 6 caracteres.',
            );
          } else {
            Alert.alert('Error', 'Debes llenar todos los campos');
          }
        }}>
        <Text style={[styles.whiteFont]}>
          {loading ? 'Cargando...' : 'Proseguir'}
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  imageContainer: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
  },
  loading: {
    backgroundColor: '#CCC',
  },
  icon: {
    fontSize: 40,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 50,
    marginTop: 5,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(98,100,207,1)',
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginBottom: 20,
    backgroundColor: 'rgba(98,100,207,1)',
    borderRadius: 50,
  },
  whiteFont: {
    textTransform: 'uppercase',
    color: '#FFF',
  },
});

const mapDispatchToProps = dispatch => ({
  setGeneral: data => dispatch({type: Actions.setGeneral, payload: data}),
});

export default connect(
  null,
  mapDispatchToProps,
)(CadastroGeral);
