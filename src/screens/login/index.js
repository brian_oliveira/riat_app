import React, {useState, useEffect} from 'react';
import Colors from '../../assets/colors';
import {
  View,
  Text,
  TextInput,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Stitch} from 'mongodb-stitch-react-native-sdk';
import {connect} from 'react-redux';
import Actions from '../../redux/actions/actions';
import {doMockLogin} from './performLogin';

const LoginPage = props => {
  const [login, setLogin] = useState({
    email: '',
    password: '',
  });

  const validateForm = form => {
    if (form.email.length > 0 && form.password.length > 0) {
      console.log('tudo certo');
      return true;
    } else {
      console.log('faltando dados');
      return false;
    }
  };

  useEffect(() => {
    if (!Stitch.hasAppClient('riat-sfhra')) {
      Stitch.initializeDefaultAppClient('riat-sfhra').then(client => {
        props.setStitchClient(client);
      });
    } else {
      console.log('App Client já setado');
    }
  }, [props]);

  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.logo}
          source={require('../../assets/logo_riat.png')}
        />
        <Image
          style={[styles.logo, {height: Dimensions.get('window').height * 0.1}]}
          source={require('../../assets/logo_riat_nome.png')}
        />
        <Text style={{marginTop: 20}}>E-mail</Text>
        <TextInput
          value={login.login}
          onChangeText={value => setLogin({...login, email: value})}
          style={styles.textInput}
          placeholder={'Ex: alberto.martinez@gmail.com'}
        />
        <Text>Contraseña</Text>
        <TextInput
          value={login.password}
          onChangeText={value => setLogin({...login, password: value})}
          secureTextEntry={true}
          style={styles.textInput}
          placeholder={'Informe su contraseña.'}
        />
        <TouchableOpacity
          onPress={() => {
            if (validateForm(login)) {
              doMockLogin(
                props,
                login.email.toLowerCase(),
                login.email.toLowerCase(),
                login.password,
              );
            } else {
              Alert.alert(
                'Completa todos los campos',
                'Debes completar todos los campos.',
              );
            }
          }}
          disabled={props.logging}
          style={[
            styles.buttonEntrar,
            props.logging ? {backgroundColor: '#A0A0A0'} : {},
          ]}>
          <Text style={[styles.whiteFont, {textTransform: 'uppercase'}]}>
            {props.logging ? 'Carregando...' : 'Entrar'}
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={[styles.buttonEntrar, {backgroundColor: Colors.colorRed}]}
        onPress={() => props.navigation.navigate('CadastroGeral')}>
        <Text style={[styles.whiteFont, {textTransform: 'uppercase'}]}>
          Registrate
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('VisitanteMenu');
        }}
        style={[styles.center]}>
        <Text>Entrar como Visitante</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.center]}>
        <Text>Informaciones Legales.</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 25,
    flex: 1,
    justifyContent: 'center',
  },
  textInput: {
    paddingLeft: 15,
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: 50,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: Colors.colorBlue,
  },
  logo: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').height * 0.2,
    resizeMode: 'contain',
    margin: 0,
    alignSelf: 'center',
  },
  whiteFont: {
    color: '#FFF',
  },
  buttonEntrar: {
    marginTop: 15,
    borderRadius: 50,
    padding: 13,
    alignItems: 'center',
    backgroundColor: Colors.colorBlue,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 3,
  },
  center: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
});

const mapStateToProps = state => ({
  stitchClient: state.general.stitchClient,
  logging: state.usuarios.logging,
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  loggingIn: () => dispatch({type: Actions.loggingIn}),
  loggedFail: () => dispatch({type: Actions.loggedFail}),
  logged: user => dispatch({type: Actions.logged, payload: user}),
  setStitchClient: client =>
    dispatch({type: Actions.setStitchClient, payload: client}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage);
