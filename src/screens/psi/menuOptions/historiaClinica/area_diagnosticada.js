import React, {useState} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Picker,
  Alert,
  TextInput,
  StyleSheet,
} from 'react-native';
import CheckBox from 'react-native-check-box';
import Colors from '../../../../assets/colors';
import {AREAS_PROMOVER} from '../../../../assets/consts';
import {connect} from 'react-redux';
import {updateSolicitacao} from '../../../../DAOs/paciente';

const createObjectArrayAreasPromover = () => {
  let array = [];

  AREAS_PROMOVER.forEach(area => {
    let obj = {title: area, checked: false};
    array.push(obj);
  });

  return array;
};

const AreaDiagnosticada = props => {
  const [areasPromover, changeAreas] = useState(
    createObjectArrayAreasPromover(),
  );

  const [plan, setPlan] = useState({
    planActividades: '',
    observaciones: '',
    diagnosticoPresuntivo: '',
  });

  const dataToServer = () => {
    let areasSelecionadas = areasPromover.filter(area => area.checked === true);

    areasSelecionadas.forEach((area, index) => {
      areasSelecionadas[index] = area.title;
    });

    if (areasSelecionadas.length === 0) {
      Alert.alert('Error', 'Debes marcar por mínimo una area a promover');
      console.log('nenhuma área a promover marcada');
    } else {
      if (props.pacienteSelected.hasOwnProperty('solicitacao')) {
        updateSolicitacao({
          solicitacaoId: props.pacienteSelected.solicitacao._id,
          changes: {
            areasPromover: areasSelecionadas,
            planActividades: plan.planActividades,
            observaciones: plan.observaciones,
            diagnosticoPresuntivo: plan.diagnosticoPresuntivo,
          },
        });
      } else {
        Alert.alert('Error', 'Paciente no tiene solicitacion');
      }
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.textPatient}>
        {props.pacienteSelected.nombreCompleto}
      </Text>
      <Text style={styles.textPromover}>Areas a Promover:</Text>
      {areasPromover
        ? areasPromover.map((areaPromover, index, array) => (
            <CheckBox
              checkedCheckBoxColor={Colors.colorBlue}
              isChecked={areasPromover[index].checked}
              rightText={areaPromover.title}
              onClick={() => {
                let newArray = [...array];
                newArray[index] = {
                  ...newArray[index],
                  checked: !newArray[index].checked,
                };
                changeAreas(newArray);
              }}
            />
          ))
        : null}

      <Text style={styles.title}>Diagnostico Presuntivo</Text>
      <TextInput
        style={styles.textInput}
        placeholder="Informe aquí el diagnostico presuntivo"
        multiline
        numberOfLines={7}
        onChangeText={text => setPlan({...plan, diagnosticoPresuntivo: text})}
      />
      <Text style={styles.title}>Plan de actividades</Text>
      <TextInput
        style={styles.textInput}
        placeholder="Describa aqui el plano de actividades"
        multiline
        numberOfLines={7}
        onChangeText={text => setPlan({...plan, planActividades: text})}
      />
      <Text style={styles.title}>Observaciones (Opcional)</Text>
      <TextInput
        style={styles.textInput}
        placeholder="Describa  las observaciones"
        multiline
        numberOfLines={7}
        onChangeText={text => setPlan({...plan, observaciones: text})}
      />
      <TouchableOpacity style={styles.button} onPress={() => dataToServer()}>
        <Text style={styles.buttonText}>Enviar</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  textPatient: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  textPromover: {
    fontSize: 16,
    fontWeight: '700',
    color: Colors.colorBlue,
    marginBottom: 5,
  },
  button: {
    marginTop: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    borderRadius: 4,
    backgroundColor: Colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
  },
  buttonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFF',
    textTransform: 'uppercase',
  },
});

const mapStateToProps = state => ({
  pacienteSelected: state.paciente.pacienteSelected,
});

export default connect(mapStateToProps)(AreaDiagnosticada);
