import React from 'react';
import {
  FlatList,
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import Actions from '../../../../redux/actions/actions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';

const HistoriaClinicaPacientes = props => {
  const [search, setSearchString] = React.useState({
    patientSearch: '',
  });

  return (
    <View style={styles.container}>
      <View style={styles.searchForm}>
        <TextInput
          style={styles.searchInput}
          placeholder={'Buscar paciente...'}
          placeholderTextColor="#999"
          value={search.patientSearch}
          onChangeText={value =>
            setSearchString({...search, patientSearch: value})
          }
        />
        <TouchableOpacity style={styles.searchButton}>
          <Icon name="search" size={20} color="#999" />
        </TouchableOpacity>
      </View>
      <FlatList
        data={props.pacientes}
        keyExtractor={item => `${item._id.$oid}`}
        renderItem={({item}) => (
          <View style={{marginBottom: 15}}>
            <View style={styles.patientContainer}>
              <Text style={styles.patientTextName}>
                {item.nombreCompleto}
                {/*<Text style={{fontWeight: 'normal'}}>{`, ${props.age}`}</Text>*/}
              </Text>
            </View>
            <TouchableOpacity
              disabled={!item.hasOwnProperty('solicitacao')}
              style={[
                styles.profileButton,
                !item.hasOwnProperty('solicitacao')
                  ? {backgroundColor: '#CCC'}
                  : {},
              ]}
              onPress={() => {
                props.setPacienteGeral(item);
                props.navigation.navigate('AreaDiagnosticada');
              }}>
              <Text style={styles.profileButtonText}>
                {item.hasOwnProperty('solicitacao')
                  ? 'areasPromover' in item.solicitacao
                    ? 'Actualizar Historia'
                    : 'Hacer Historia'
                  : 'Sin solicitación'}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginTop: 5,
  },
  footer: {
    position: 'absolute',
    right: 15,
    bottom: 15,
    alignItems: 'flex-end',
  },
  addPatientButton: {
    backgroundColor: Colors.colorRed,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
  },
  searchForm: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  searchInput: {
    flex: 1,
    height: 40,
    backgroundColor: '#eee',
    borderRadius: 4,
    marginLeft: 10,
    paddingRight: 12,
    color: '#999',
  },
  searchButton: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    borderRadius: 4,
    marginLeft: 10,
  },
  patientContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  patientThumbnail: {
    width: 64,
    height: 64,
    borderRadius: 32,
    backgroundColor: '#eee',
  },
  patientTextName: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  patientAddressText: {
    textAlign: 'center',
  },
  profileButton: {
    marginTop: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    borderRadius: 4,
    backgroundColor: Colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
  },
  profileButtonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFF',
    textTransform: 'uppercase',
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  pacientes: state.paciente.pacientes,
});

const mapDispatchToProps = dispatch => ({
  setPacienteGeral: paciente =>
    dispatch({type: Actions.setPacienteGeral, payload: paciente}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HistoriaClinicaPacientes);
