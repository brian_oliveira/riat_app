/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
} from 'react-native';
import {LineChart} from 'react-native-chart-kit';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';
import {connect} from 'react-redux';
import {getAge} from '../../../../assets/utils';
import {getSessoesFromPaciente} from "../../../../DAOs/paciente";

export const Card = props => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardHeaderContainer}>
        <Text style={styles.cardTextHeader}>{props.title}</Text>
      </View>
      <View style={styles.cardBodyContainer}>
        <Text>{props.content}</Text>
      </View>
    </View>
  );
};

const PacienteDetalhePsi = props => {
  const prepareStringDatosClinicos = () => {
    let string = 'Areas a promover: \n';
    if ('areasPromover' in props.pacienteSelected.solicitacao) {
      props.pacienteSelected.solicitacao.areasPromover.forEach(area => {
        string += area + '\n';
      });
      string += '\n';
      string += 'Observaciones:\n';
      string += props.pacienteSelected.solicitacao.observaciones + '\n';
      string += '\n';
      string += 'Plan de Actividades:\n';
      string += props.pacienteSelected.solicitacao.planActividades;
    } else {
      string += 'Todavía no ha sido hecha la historia clinica.';
    }

    return string;
  };

  const prepareStringEquipoMedico = () => {
    let string = '';
    string += 'Médicos: \n';
    props.pacienteSelected.medicos.forEach(
      medico => (string += medico.nombre + '\n'),
    );
    string += '\nAcompanhantes: \n';
    props.pacienteSelected.acompanhantes.forEach(
      acompanhante => (string += acompanhante.nombre + '\n'),
    );
    return string;
  };

  React.useEffect(() => {
    setLoading(true);
    getSessoesFromPaciente(
        props.pacienteSelected._id,
        res => {
          setInformesPaciente(res.data);
          setLoading(false);
        },
        () => {
          alert('Error al recibir informes');
          setLoading(false);
        },
    );
  }, [props.pacienteSelected._id]);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.containerProfile}>
        <Image
          style={styles.profileImage}
          source={{
            uri: `https://riat.s3.amazonaws.com/public/${
              props.pacienteSelected._id.$oid
            }_paciente_userprofile_image`,
            // 'https://images.unsplash.com/photo-1548365108-908055adb1c8?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          }}
        />
        <Text style={styles.name}>
          {props.pacienteSelected.nombreCompleto},{' '}
          <Text style={{fontWeight: 'normal'}}>
            {getAge(new Date(props.pacienteSelected.fechaDeNacimiento))}
          </Text>
        </Text>
        <Text numberOfLines={2}>{props.pacienteSelected.direccion}</Text>
        <Text>Status: Activo</Text>
      </View>
      <LineChart
        data={{
          labels: ['January', 'February', 'March', 'April', 'May', 'June'],
          datasets: [
            {
              data: [
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
              ],
            },
          ],
        }}
        width={Dimensions.get('window').width - 20} // from react-native
        height={220}
        yAxisLabel={'$'}
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: Colors.colorLightBlue,
          backgroundGradientTo: Colors.colorBlue,
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: '3',
            strokeWidth: '1',
            stroke: Colors.colorBlue,
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      />
      <Card
        title="Equipo médico y de acompañamiento"
        content={prepareStringEquipoMedico()}
      />
      <Card
        title="Datos clinicos y Recomendaciones"
        content={prepareStringDatosClinicos()}
      />
      <TouchableOpacity
        onPress={() => props.navigation.navigate('ChatRooms')}
        style={styles.button}>
        <Icon name="comments" size={20} color="#FFF" />
        <Text style={styles.buttonText}>Chat</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          props.navigation.navigate('PacienteInformes');
        }}>
        <Icon name="file" size={20} color="#FFF" />
        <Text style={styles.buttonText}>Informes</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20,
  },
  containerProfile: {
    alignItems: 'center',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 10,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  cardContainer: {
    flex: 1,
    width: '100%',
    borderColor: '#eee',
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 10,
  },
  cardHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 3,
    height: 48,
  },
  cardBodyContainer: {
    flex: 1,
    padding: 5,
  },
  cardTextHeader: {
    fontSize: 16,
    color: '#FFF',
  },
  button: {
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 1,
    flexDirection: 'row',
    height: 36,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  buttonText: {
    fontSize: 14,
    color: '#FFF',
    marginLeft: 5,
  },
});

const mapStateToProps = state => ({
  pacienteSelected: state.paciente.pacienteSelected,
});

export default connect(mapStateToProps)(PacienteDetalhePsi);
