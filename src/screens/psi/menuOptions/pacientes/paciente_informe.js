import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from '../../../../assets/colors';
import {Text, View, ScrollView} from 'react-native';
import {connect} from 'react-redux';

const PacienteInforme = props => {
  return (
    <ScrollView style={styles.container}>
      <Text>Registro del Acompañante:</Text>
      {props.sessaoSelected.informeDone ? (
        props.sessaoSelected.relatorio.registroAcompanante.map(registro => (
          <View style={{display: 'flex', flexDirection: 'row'}}>
            <Text>{registro.registro}</Text>
            <Text>{registro.nota.$numberInt}</Text>
          </View>
        ))
      ) : (
        <Text>Todavía no hay un informe hecho de esa sesión.</Text>
      )}
      <Text>Objetivos del AT</Text>
      {props.sessaoSelected.informeDone ? (
        props.sessaoSelected.relatorio.areasPromover.map(area => (
          <View>
            <Text>{area.area}</Text>
            <Text>{area.nota.$numberInt}</Text>
          </View>
        ))
      ) : (
        <></>
      )}
      {props.sessaoSelected.informeDone ? (
        <View>
          <Text>Observaciones</Text>
          <Text>{props.sessaoSelected.relatorio.observaciones}</Text>
        </View>
      ) : (
        <></>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
    backgroundColor: '#F7F7F7',
  },
  centerAlign: {
    textAlign: 'center',
    color: Colors.colorRed,
    marginTop: 15,
    marginBottom: 8,
    fontWeight: 'bold',
    fontSize: 18,
  },
  textInput: {
    backgroundColor: '#FFF',
    borderRadius: 7,
    paddingLeft: 15,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    flex: 1,
    elevation: 4,
  },
  pacienteContainer: {
    backgroundColor: Colors.colorBlue,
    padding: 18,
    borderRadius: 50,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 10,
  },
  pacienteName: {
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  sessaoSelected: state.profissional.sessaoSelected,
});

export default connect(mapStateToProps)(PacienteInforme);
