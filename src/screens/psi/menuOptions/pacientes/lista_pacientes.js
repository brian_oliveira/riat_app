import React from 'react';
import {connect} from 'react-redux';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';
import Actions from '../../../../redux/actions/actions';

const ListaPacientesPsi = props => {
  React.useEffect(() => {
    console.log(props.pacientes);
  });
  return (
    <ScrollView style={styles.container}>
      <View style={{flexDirection: 'row', marginTop: 25, marginBottom: 20}}>
        <Icon
          name={'search'}
          style={{marginTop: 15, marginRight: 10}}
          size={18}
        />
        <TextInput style={styles.textInput} placeholder={'Buscar'} />
      </View>
      <Text style={[styles.centerAlign]}>Pacientes</Text>
      {props.pacientes && props.pacientes.length > 0 ? (
        props.pacientes.map((paciente, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => {
              props.setPacienteGeral(paciente);
              props.navigation.navigate('PacienteDetalhePsi');
            }}
            style={styles.pacienteContainer}>
            <Text style={styles.pacienteName}>{paciente.nombreCompleto}</Text>
          </TouchableOpacity>
        ))
      ) : (
        <Text>Ningún paciente agregado todavía.</Text>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F7F7F7',
  },
  centerAlign: {
    textAlign: 'center',
    color: Colors.colorRed,
    marginTop: 15,
    marginBottom: 8,
    fontWeight: 'bold',
    fontSize: 18,
  },
  textInput: {
    backgroundColor: '#FFF',
    borderRadius: 7,
    paddingLeft: 15,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    flex: 1,
    elevation: 4,
  },
  pacienteContainer: {
    backgroundColor: Colors.colorBlue,
    padding: 18,
    borderRadius: 50,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 10,
  },
  pacienteName: {
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  pacientes: state.paciente.pacientes,
});

const mapDispatchToProps = dispatch => ({
  setPacienteGeral: paciente =>
    dispatch({type: Actions.setPacienteGeral, payload: paciente}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListaPacientesPsi);
