import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';
import {connect} from 'react-redux';
import {getAge} from '../../../../assets/utils';
import {getSessoesFromPaciente} from '../../../../DAOs/paciente';
import moment from 'moment';
import Actions from '../../../../redux/actions/actions';

const PacienteInformes = props => {
  const [loading, setLoading] = React.useState(false);
  const [informesPaciente, setInformesPaciente] = React.useState([]);

  React.useEffect(() => {
    setLoading(true);
    getSessoesFromPaciente(
      props.pacienteSelected._id,
      res => {
        setInformesPaciente(res.data);
        setLoading(false);
      },
      () => {
        alert('Error al recibir informes');
        setLoading(false);
      },
    );
  }, [props.pacienteSelected._id]);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.containerProfile}>
        <Text style={styles.name}>
          {props.pacienteSelected.nombreCompleto},{' '}
          <Text style={{fontWeight: 'normal'}}>
            {getAge(new Date(props.pacienteSelected.fechaDeNacimiento))}
          </Text>
        </Text>
        <Text numberOfLines={2}>{props.pacienteSelected.direccion}</Text>
      </View>
      {informesPaciente.map(informe => (
          <TouchableOpacity
          onPress={() => {
          props.selectSessao(informe);
          props.navigation.navigate('PacienteInforme');
      }}
          style={styles.button}>
          <View>
          <Text style={styles.buttonText}>{
          'De ' +
          moment(new Date(informe.horaInicio)).format('DD/MM/YYYY') +
          ' | De: ' +
          moment(new Date(informe.horaInicio)).format('HH[h]') +
          '-' +
          moment(new Date(informe.horarioFim)).format('HH[h]')
      }</Text>
          <Text style={styles.buttonText}>
          {'Acompanhante: ' + informe.acompanhante.nombre}
          </Text>
          </View>
          </TouchableOpacity>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20,
  },
  containerProfile: {
    alignItems: 'center',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 10,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  cardContainer: {
    flex: 1,
    width: '100%',
    borderColor: '#eee',
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 10,
  },
  cardHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 3,
    height: 48,
  },
  cardBodyContainer: {
    flex: 1,
    padding: 5,
  },
  cardTextHeader: {
    fontSize: 16,
    color: '#FFF',
  },
  button: {
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 1,
    flexDirection: 'row',
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  buttonText: {
    fontSize: 14,
    color: '#FFF',
    marginLeft: 5,
  },

  readByContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginLeft: 5,
  },
  readByText: {
    color: '#FFF',
    fontSize: 10,
    marginLeft: 5,
  },
});

const mapStateToProps = state => ({
  pacienteSelected: state.paciente.pacienteSelected,
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  selectSessao: sessao =>
    dispatch({type: Actions.selectSessao, payload: sessao}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PacienteInformes);
