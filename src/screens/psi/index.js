import React, {useEffect} from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../assets/colors';
import {connect} from 'react-redux';
import Actions from '../../redux/actions/actions';

//TODO: Objetivos a trabajar ao invés de Áreas a Promover

const PsiMenu = props => {
  const options = [
    {
      text: 'Historia Clinica',
      image: 'newspaper',
      local: 'HistoriaClinica',
    },
    {
      text: 'Servicios',
      image: 'newspaper',
      local: 'Acompanamientos',
    },
    {
      text: 'Pacientes',
      image: 'users',
      local: 'ListaPacientesPsi',
    },
    {
      text: 'Chat',
      image: 'comments',
      local: 'ChatRooms',
    },
  ];

  useEffect(() => {
    props.setPacientesGeral(props.userLogged.pacientes);
  });

  return (
    <ImageBackground
      source={require('../../assets/splash_screen.png')}
      style={[{width: '100%', height: '100%'}]}
      imageStyle={{opacity: 0.3}}>
      <View>
        <Image
          style={[styles.logo, {marginTop: 38}]}
          source={require('../../assets/logo_riat.png')}
        />
        <Image
          style={[styles.logo, {height: Dimensions.get('window').height * 0.1}]}
          source={require('../../assets/logo_riat_nome.png')}
        />
        <View style={styles.row}>
          {options.map((option, index) => (
            <TouchableOpacity
              style={styles.iconOption}
              onPress={() => props.navigation.navigate(option.local)}>
              <Icon name={option.image} size={50} color={Colors.colorBlue} />
              <Text
                style={{
                  fontSize: 15,
                  textAlign: 'center',
                  marginTop: 10,
                  color: Colors.colorRed,
                }}>
                {option.text}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginTop: 30,
  },
  logo: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').height * 0.2,
    resizeMode: 'contain',
    margin: 0,
    alignSelf: 'center',
  },
  img: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  iconOption: {
    width: Dimensions.get('window').width * 0.33,
    padding: 10,
    alignItems: 'center',
  },
  icon: {
    fontSize: 30,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  setPacientesGeral: pacientes =>
    dispatch({type: Actions.setPacientesGeral, payload: pacientes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PsiMenu);
