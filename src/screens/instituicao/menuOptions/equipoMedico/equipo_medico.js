import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Colors from '../../../../assets/colors';
import Actions from '../../../../redux/actions/actions';
import {connect} from 'react-redux';

const Medico = props => {
  return (
    <View style={{marginBottom: 15}}>
      <View style={styles.patientContainer}>
        {/*<Image style={styles.patientThumbnail} source={{uri: props.avatar}} />*/}
        <Text style={styles.patientTextName}>
          {props.nombre}
          {/*<Text style={{fontWeight: 'normal'}}>{`, ${props.age}`}</Text>*/}
        </Text>
        <Text style={{color: Colors.colorBlue}}>{props.tipoProfesional}</Text>
        {/* TODO: Trocar endereço por email e telefone */}
        {/*<Text numberOfLines={2} style={styles.patientAddressText}>*/}
        {/*  {props.address}*/}
        {/*</Text>*/}
      </View>
      <TouchableOpacity
        style={styles.profileButton}
        onPress={() => props.navigation.navigate('MedicoDetalhe', {data: props.medicoData})}>
        <Text style={styles.profileButtonText}>Ver</Text>
      </TouchableOpacity>
    </View>
  );
};

const MedicoInstituicao = props => {
  const [search, setSearchString] = useState({
    patientSearch: '',
  });

  console.log(props.medicos);

  return (
    <View style={styles.container}>
      <View style={styles.searchForm}>
        <TextInput
          style={styles.searchInput}
          placeholder={'Buscar medico...'}
          placeholderTextColor="#999"
          value={search.patientSearch}
          onChangeText={value =>
            setSearchString({...search, patientSearch: value})
          }
        />
        <TouchableOpacity style={styles.searchButton}>
          <Icon name="search" size={20} color="#999" />
        </TouchableOpacity>
      </View>
      <FlatList
        style={{marginBottom: 10}}
        data={props.medicos}
        keyExtractor={item => `${item.id}`}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <Medico
            navigation={props.navigation}
            // avatar={item.avatar}
            nombre={item.nombre}
            // age={item.age}
            // address={item.address}
            tipoProfesional={item.tipoProfesional}
            medicoData={item}
          />
        )}
      />
      <View style={styles.footer}>
        <TouchableOpacity
          style={styles.addPatientButton}
          onPress={() => props.navigation.navigate('AdicionarMedico')}>
          <Icon name="user-md" size={20} color="#FFF" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 5,
  },
  footer: {
    position: 'absolute',
    right: 15,
    bottom: 15,
    alignItems: 'flex-end',
  },
  addPatientButton: {
    backgroundColor: Colors.colorRed,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
  },
  searchForm: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  searchInput: {
    flex: 1,
    height: 40,
    backgroundColor: '#eee',
    borderRadius: 4,
    marginLeft: 10,
    paddingRight: 12,
    color: '#999',
  },
  searchButton: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    borderRadius: 4,
    marginLeft: 10,
  },
  patientContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  patientThumbnail: {
    width: 64,
    height: 64,
    borderRadius: 32,
    backgroundColor: '#eee',
  },
  patientTextName: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  patientAddressText: {
    textAlign: 'center',
  },
  profileButton: {
    marginTop: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    borderRadius: 4,
    backgroundColor: Colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
  },
  profileButtonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFF',
    textTransform: 'uppercase',
  },
});

const mapStateToProps = state => ({
  medicos: state.instituicao.medicos,
});

const mapDispatchToProps = dispatch => ({
  setMedicos: medicos => dispatch({type: Actions.setMedicos, payload: medicos}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MedicoInstituicao);
