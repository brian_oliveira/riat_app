import React, {useState} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  Picker,
  Alert,
} from 'react-native';
import {newMedico, sendMail} from '../../../../DAOs/instituicao';
import {connect} from 'react-redux';
import Actions from '../../../../redux/actions/actions';

const AdicionarMedico = props => {
  const [datosProfisional, setDatos] = useState({
    nombre: '',
    tipoProfesional: 'psicologo',
    matriculaNacional: '',
    matriculaProvincial: '',
    email: '',
    especialidade: '',
  });

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textLabel}>Nombre del Médico</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={text => setDatos({...datosProfisional, nombre: text})}
        placeholder={'Ejemplo: Carlos Contreras'}
      />
      <Text style={styles.textLabel}>Tipo de Profesional</Text>
      <Picker
        selectedValue={datosProfisional.tipoProfesional}
        onValueChange={itemValue =>
          setDatos({...datosProfisional, tipoProfesional: itemValue})
        }>
        <Picker.Item value={'psicologo'} label={'Psicologo'} />
        <Picker.Item value={'psiquiatra'} label={'Psiquiatra'} />
        <Picker.Item value={'psicanalista'} label={'Psicanalista'} />
        <Picker.Item value={false} label={'Otro...'} />
      </Picker>
      {datosProfisional.tipoProfesional ? (
        <></>
      ) : (
        <View>
          <Text>Cúal?</Text>
          <TextInput
            onChangeText={text =>
              setDatos({...datosProfisional, tipoProfesional: text})
            }
            style={styles.textInput}
            placeholder={'Ejemplo: Neuropsicologo'}
          />
        </View>
      )}

      <Text style={styles.textLabel}>Especialidade</Text>
      <TextInput
        onChangeText={text =>
          setDatos({...datosProfisional, especialidade: text})
        }
        style={styles.textInput}
        placeholder={'Ejemplo: Depresión aguda'}
      />

      <Text style={styles.textLabel}>Matricula Nacional</Text>
      <TextInput
        onChangeText={text =>
          setDatos({...datosProfisional, matriculaNacional: text})
        }
        style={styles.textInput}
        placeholder={'Ejemplo: 2382938'}
      />
      <Text style={styles.textLabel}>Matricula Provincial</Text>
      <TextInput
        onChangeText={text =>
          setDatos({...datosProfisional, matriculaProvincial: text})
        }
        style={styles.textInput}
        placeholder={'Ejemplo: 2382938'}
      />
      <Text style={styles.textLabel}>E-mail</Text>
      <TextInput
        onChangeText={text =>
          setDatos({...datosProfisional, email: text.toLowerCase()})
        }
        style={styles.textInput}
        placehodler={'Ejemplo: carlos.contreras@gmail.com'}
      />
      <TouchableOpacity
        style={styles.btnNext}
        onPress={() => {
          newMedico(
            {
              ...datosProfisional,
              instituicao: props.userLogged.instituicao._id,
            },
            props,
          );
        }}>
        <Text style={styles.uppercaseAndWhite}>Enviar Solicitación</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  uppercaseAndWhite: {
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    borderRadius: 50,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(98,100,207,1)',
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 15,
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: 'rgba(98,100,207,1)',
    borderRadius: 50,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  setMedicos: medicos => dispatch({type: Actions.setMedicos, payload: medicos}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AdicionarMedico);
