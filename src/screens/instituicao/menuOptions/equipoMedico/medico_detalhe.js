import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView, TouchableOpacity} from 'react-native';
import Colors from '../../../../assets/colors';

import Icon from 'react-native-vector-icons/FontAwesome5';

const Item = props => {
  return (
    <View style={styles.itemContainter}>
      <Text style={styles.label}>{props.label}</Text>
      <Text style={styles.itemText}>{props.labelValue}</Text>
    </View>
  );
}

const MedicoDetalhe = props => {
  const data = props.navigation.getParam('data');
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={{alignItems: 'center'}}>
        <Image style={styles.profileImage} source={{uri: 'none'}}/>
      </View>
      <Item label="Nombre Completo: " labelValue={data.nombre} />
      <Item label="Profissão: " labelValue={data.tipoProfesional} />
      <Item label="Especialidad: " labelValue={data.especialidade ? data.especialidade : 'No disponible'} />
      <Item label="Matricula Nacional: " labelValue={data.matriculaNacional ? data.matriculaNacional : 'No disponible'} />
      <Item label="Matricula Provincial: " labelValue={data.matriculaProvincial ? data.matriculaProvincial : 'No disponible'} />

      <Item label="DNI: " labelValue={data.dni ? data.dni : 'No disponible'} />
      <Item label="Fecha de Nascimento: " labelValue={data.dataNasc ? data.dataNasc : 'No disponible'} />
      <Item label="Endereço: " labelValue={data.endereco ? data.endereco : 'No disponible'} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
  },
  itemContainter: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  label: {
    fontSize: 16,
    marginHorizontal: 15,
  },
  itemText: {
    fontSize: 14,
    textAlign: 'left',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 20,
  },
  editButton: {
    width: 30,
    backgroundColor: Colors.colorRed,
    borderRadius: 30,
    padding: 5,
    marginTop: 15,
  },
});

export default MedicoDetalhe;
