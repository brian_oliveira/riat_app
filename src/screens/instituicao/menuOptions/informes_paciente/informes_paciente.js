import React from 'react';
import {View, Image, TouchableOpacity, Text, StyleSheet, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';

const Informe = props => {
  return (
    <TouchableOpacity style={styles.button}>
      <View>
        <Text style={styles.buttonText}>{props.schedule}</Text>
        <Text style={styles.buttonText}>
          {`Acompanhante: ${props.professional}`}{' '}
        </Text>
      </View>
      {props.read ? (
        <View style={styles.readByContainer}>
          <Icon name="check" size={10} color="#FFF" />
          <Text style={styles.readByText}>Lido</Text>
        </View>
      ) : null}
    </TouchableOpacity>
  );
}

const informesPaciente = props => {
  const id = props.navigation.getParam('id');
  const nombreCompleto = props.navigation.getParam('nombreCompleto');

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.containerProfile}>
        <Image
          style={styles.profileImage}
          source={{
            uri: `https://riat.s3.amazonaws.com/public/${id}_paciente_userprofile_image`,
          }}
        />
        <Text style={styles.name}>
          {nombreCompleto},<Text style={{fontWeight: 'normal'}}>23</Text>
        </Text>
        <Text numberOfLines={2}>Buenos aires, Argentina</Text>
      </View>

      <Informe
        schedule="De 25/08/2018 | Horário: 14-18h"
        professional="German Salazar"
        read={false}
      />
      <Informe
        schedule="De 24/08/2018 | Horário: 14-18h"
        professional="German Salazar"
        read={true}
      />
      <Informe
        schedule="De 23/08/2018 | Horário: 14-18h"
        professional="German Salazar"
        read={true}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20,
  },
  containerProfile: {
    alignItems: 'center',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 10,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  cardContainer: {
    flex: 1,
    width: '100%',
    borderColor: '#eee',
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 10,
  },
  cardHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 3,
    height: 48,
  },
  cardBodyContainer:{
    flex: 1,
    padding: 5,
  },
  cardTextHeader: {
    fontSize: 16,
    color: '#FFF',
  },
  button: {
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 1,
    flexDirection: 'row',
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  buttonText: {
    fontSize: 14,
    color: '#FFF',
    marginLeft: 5,
  },

  readByContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginLeft: 5,

  },
  readByText: {
    color: '#FFF',
    fontSize: 10,
    marginLeft: 5,
  },
});
export default informesPaciente;
