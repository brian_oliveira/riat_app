import React from 'react';
import {View, ScrollView, Text, StyleSheet, Picker} from 'react-native';
import Colors from '../../../../assets/colors';
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux';
import {
  calcularHorasTrabalhadas,
  returnHorasTrabalhadas,
} from '../../../../assets/utils';

const TrabajoComponent = props => {
  return (
    <View style={styles.containerTrabajo}>
      <Text style={{fontSize: 20, textTransform: 'uppercase'}}>
        {props.sessao.paciente.nombreCompleto}
      </Text>
      <Text>Atendido por: {props.sessao.acompanhante.nombre}</Text>
      <Text>
        Horas Trabajadas:{' '}
        {calcularHorasTrabalhadas(
          props.sessao.horaInicio,
          props.sessao.horarioFim,
        ) + '\n'}
       Valor de la hora: {props.userLogged.valorHora  ? props.userLogged.valorHora + '\n' : 'No disponible\n'}
        Monto Total: $
        {props.userLogged.valorHora ? returnHorasTrabalhadas(
          props.sessao.horaInicio,
          props.sessao.horarioFim,
        ) * parseFloat(props.userLogged.valorHora): ' No disponible\n' }
      </Text>
      {/*<Text>Estimativa de Pago: 21/05/2019</Text>*/}
    </View>
  );
};

const Administrativo = props => {
  // TODO: FAZER UM MAPBOX COM TODOS OS CHECK IN E CHECK OUT

  const getValorTotal = () => {
    let array = [];
    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    props.sessoes.forEach(sessao => {
      array.push(
        returnHorasTrabalhadas(sessao.horaInicio, sessao.horarioFim) *
          parseFloat(props.userLogged.valorHora),
      );
    });
    return array.reduce(reducer);
  };

  return (
    <ScrollView style={styles.container}>
      {/*<Picker style={{marginTop: 15}}>*/}
      {/*  <Picker.Item label={'Agosto / 2019'} />*/}
      {/*  <Picker.Item label={'Septiembre / 2019'} />*/}
      {/*</Picker>*/}
      <LinearGradient
        style={styles.header}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[Colors.colorBlue, Colors.colorLightBlue]}>
        <Text style={[styles.headerText, {fontSize: 15}]}>A Pagar:</Text>
        <Text style={[styles.headerText, {fontSize: 45}]}>
          { props.userLogged.valorHora  ? `$${getValorTotal(props.sessoes, props.userLogged.valorHora)}` : ` No disponible`}
        </Text>
        <Text style={[styles.headerText]}>Horas trabajadas: 300h</Text>
      </LinearGradient>
      <Text
        style={{
          textTransform: 'uppercase',
          color: '#AAA',
          marginTop: 15,
          marginBottom: 10,
          marginLeft: 15,
          fontSize: 20,
        }}>
        Detalles
      </Text>
      {props.sessoes.map(sessao => (
        <TrabajoComponent sessao={sessao} userLogged={props.userLogged} />
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  header: {
    justifyContent: 'center',
    marginTop: 20,
    shadowColor: '#000',
    marginLeft: 15,
    marginRight: 15,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    padding: 15,
    borderRadius: 10,
  },
  headerText: {
    textTransform: 'uppercase',
    textAlign: 'center',
    color: '#FFFFFF',
  },
  containerTrabajo: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    borderRadius: 5,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    padding: 10,
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  sessoes: state.instituicao.sessoes,
});

export default connect(mapStateToProps)(Administrativo);
