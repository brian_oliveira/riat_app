import React, {useState} from 'react';
import {Picker, ScrollView, Text, Image, View, StyleSheet} from 'react-native';

const AdministrativoPaciente = props => {
  return (
    <ScrollView style={styles.container}>
      <Image
        style={styles.patientThumbnail}
        source={{
          uri:
            'https://images.unsplash.com/photo-1548365108-908055adb1c8?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
        }}
      />
      <Text
        style={{fontSize: 17, textTransform: 'uppercase', alignSelf: 'center'}}>
        Leandro Schmidt
      </Text>
      <Text style={{fontSize: 14, alignSelf: 'center'}}>
        5 sessões até o momento
      </Text>
      <Picker>
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
      </Picker>
      <View style={styles.row}>
        <Text>Total a Pagar:</Text>
        <Text style={{flex: 1, textAlign: 'right'}}>$4.500,00</Text>
      </View>
      <View style={styles.row}>
        <Text>Horas Recebidas:</Text>
        <Text style={{flex: 1, textAlign: 'right'}}>135H</Text>
      </View>
      <View style={styles.row}>
        <Text>Valor por Hora:</Text>
        <Text style={{flex: 1, textAlign: 'right'}}>$150,00</Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    paddingTop: 20,
  },
  patientThumbnail: {
    marginTop: 30,
    marginBottom: 15,
    width: 80,
    alignSelf: 'center',
    height: 80,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    shadowColor: '#000',
    borderRadius: 40,
    backgroundColor: '#eee',
  },
});

export default AdministrativoPaciente;
