/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';
import Actions from '../../../../redux/actions/actions';
import {connect} from 'react-redux';
import {URL} from '../../../../assets/consts';
import axios from 'axios';
import {getPacientes} from '../../../../DAOs/instituicao';

const checkIfFamiliarOrInst = props => {
  let pacientes = [];

  if (props.userLogged.userType === 'familiar') {
    pacientes = [...props.pacientesFam];
  } else if (props.userLogged.userType === 'funcionario') {
    pacientes = [...props.pacientes];
  }

  return pacientes.map(paciente => (
    <TouchableOpacity
      onPress={() =>
        props.navigation.navigate('AcompanamientosDetalhe', paciente)
      }
      style={styles.pacienteContainer}>
      <Text style={styles.pacienteName}>{paciente.nombreCompleto}</Text>
    </TouchableOpacity>
  ));
};

const Acompanamientos = props => {
  return (
    <ScrollView style={styles.container}>
      <View style={{flexDirection: 'row', marginTop: 25, marginBottom: 5}}>
        <Icon
          name={'search'}
          style={{marginTop: 15, marginRight: 10}}
          size={18}
        />
        <TextInput style={styles.textInput} placeholder={'Buscar'} />
      </View>
      <Text style={styles.centerAlign}>Pacientes</Text>
      {checkIfFamiliarOrInst(props)}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F7F7F7',
  },
  centerAlign: {
    textAlign: 'center',
    color: Colors.colorRed,
    marginTop: 15,
    marginBottom: 8,
    fontWeight: 'bold',
    fontSize: 18,
  },
  textInput: {
    backgroundColor: '#FFF',
    borderRadius: 7,
    paddingLeft: 15,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    flex: 1,
    elevation: 4,
  },
  pacienteContainer: {
    backgroundColor: Colors.colorBlue,
    padding: 18,
    borderRadius: 50,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 10,
  },
  pacienteName: {
    textAlign: 'center',
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  pacientes: state.instituicao.pacientes,
  userLogged: state.usuarios.userLogged,
  pacientesFam: state.familiar.pacientes,
});

const mapDispatchToProps = dispatch => ({
  setPacientes: pacientes =>
    dispatch({type: Actions.getPacientesInstituicao, payload: pacientes}),
  selectPacienteInstituicao: paciente =>
    dispatch({type: Actions.selectPacienteInstituicao, payload: paciente}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Acompanamientos);
