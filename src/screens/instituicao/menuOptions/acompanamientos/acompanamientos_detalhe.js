import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';

export const Card = props => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardHeaderContainer}>
        <Text style={styles.cardTextHeader}>{props.title}</Text>
      </View>
      <View style={styles.cardBodyContainer}>
        <Text>{props.content}</Text>
      </View>
    </View>
  );
};

const Acompanamientos = props => {
  // pegando parametros passados via navegação
  const id = props.navigation.getParam('_id').$oid;
  const nombreCompleto = props.navigation.getParam('nombreCompleto');
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.containerProfile}>
        <Image
          style={styles.profileImage}
          source={{
            uri: `https://riat.s3.amazonaws.com/public/${id}_paciente_userprofile_image`,
            // 'https://images.unsplash.com/photo-1548365108-908055adb1c8?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          }}
        />
        <Text style={styles.name}>
          {nombreCompleto}, <Text style={{fontWeight: 'normal'}}> 23</Text>
        </Text>
        <Text numberOfLines={2}>{props.navigation.getParam('direccion')}</Text>
        <Text>Status: Activo</Text>
      </View>

      <Card
        title="Datos de la solicitud"
        content={
          props.navigation.getParam('solicitacao').length > 0
            ? 'Diagnóstico: ' +
              props.navigation.getParam('solicitacao')[0].diagnostico
            : 'Paciente todavía no tiene un acompañamiento activo.'
        }
      />
      <Card title="Equipo médico y de acompañamiento" />
      <Card title="Datos clinicos y Recomendaciones" />

      <TouchableOpacity style={styles.button}>
        <Icon name="comments" size={20} color="#FFF" />
        <Text style={styles.buttonText}>Chat</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() =>
          props.navigation.navigate('InformesPaciente', {id, nombreCompleto})
        }>
        <Icon name="file" size={20} color="#FFF" />
        <Text style={styles.buttonText}>Informes</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button}>
        <Icon name="chart-bar" size={20} color="#FFF" />
        <Text style={styles.buttonText}>Graficos</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20,
  },
  containerProfile: {
    alignItems: 'center',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 10,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  cardContainer: {
    flex: 1,
    width: '100%',
    borderColor: '#eee',
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 10,
  },
  cardHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 3,
    height: 48,
  },
  cardBodyContainer: {
    flex: 1,
    padding: 5,
  },
  cardTextHeader: {
    fontSize: 16,
    color: '#FFF',
  },
  button: {
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 1,
    flexDirection: 'row',
    height: 36,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  buttonText: {
    fontSize: 14,
    color: '#FFF',
    marginLeft: 5,
  },
});

export default Acompanamientos;
