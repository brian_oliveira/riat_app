import React, {useState} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  Picker,
  StyleSheet,
} from 'react-native';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import axios from 'axios';
import {URL, IVA} from '../../assets/consts';
import Colors from '../../assets/colors';
import {
  Stitch,
  UserPasswordAuthProviderClient,
} from 'mongodb-stitch-react-native-sdk';

const CadastroInstituicao = props => {
  const [instituicaoData, setData] = useState({
    nombreInstituicion: '',
    direccion: '',
    telefono: '',
    cuit: '',
    director: '',
    iva: IVA[0],
  });

  const [loading, setLoading] = useState(false);

  const [funcionarioData, setFuncionarioData] = useState({
    nombre: '',
    email: '',
    contrasena: '',
  });

  const sendDataToServer = (data, callbackSuccess, callbackLoading) => {
    let url = URL + 'instituicao/incoming_webhook/createInstituicao';
    callbackLoading();
    axios
      .post(url, data)
      .then(() => {
        const emailPasswordClient = Stitch.defaultAppClient.auth.getProviderClient(
          UserPasswordAuthProviderClient.factory,
        );

        emailPasswordClient
          .registerWithEmail(
            data.funcionario.usuario.email,
            data.funcionario.usuario.password,
          )
          .then(() => {
            Alert.alert(
              'Bienvenido a RIAT!',
              'Usted completó su registro! Verifique su email.',
            );
            callbackSuccess();
            props.navigation.navigate('Home');
          })
          .catch(err => alert('Error registering new user: ' + err));
      })
      .catch(err => {
        Alert.alert('Perdón!', 'Aconteció un error... ' + err + 'URL: ' + url);
      });
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textLabel}>Nombre de la Instituición</Text>
      <TextInput
        onChangeText={text =>
          setData({...instituicaoData, nombreInstituicion: text})
        }
        placeholder={'Ejemplo: Clinica Salud'}
        style={styles.textInput}
      />
      <Text style={styles.textLabel}>Dirección</Text>
      <TextInput
        onChangeText={text => setData({...instituicaoData, direccion: text})}
        placeholder={'Ejemplo: Calle La Rioja, n. 12345'}
        style={styles.textInput}
      />
      <Text style={styles.textLabel}>Teléfono</Text>
      <TextInput
        onChangeText={text => setData({...instituicaoData, telefono: text})}
        placeholder={'Ejemplo: 12 345 6789'}
        style={styles.textInput}
      />
      <Text style={styles.textLabel}>CUIT</Text>
      <TextInput
        onChangeText={text => setData({...instituicaoData, cuit: text})}
        placeholder={'Informe el CUIT de la instituición.'}
        style={styles.textInput}
      />
      <Text style={styles.textLabel}>Condicción en frente al IVA</Text>
      <Picker
        selectedValue={instituicaoData.iva}
        onValueChange={value => setData({...instituicaoData, iva: value})}>
        {IVA.map(iva => (
          <Picker.Item label={iva} value={iva} />
        ))}
      </Picker>
      <Text style={styles.textLabel}>Responsable</Text>
      <TextInput
        onChangeText={text => setData({...instituicaoData, director: text})}
        placeholder={'Ejemplo: Director Brian Gonzalez'}
        style={styles.textInput}
      />
      <TouchableOpacity
        disabled={loading}
        onPress={() => {
          let data = {...props.generalData};
          delete data.imageUri;
          let instituicaoFinal = {
            ...instituicaoData,
            funcionario: {
              ...data,
              usuario: {
                email: data.email.toLowerCase(),
                password: data.contrasena,
                type: 'funcionario',
              },
            },
          };
          props.setInstituicao(instituicaoFinal);
          sendDataToServer(
            instituicaoFinal,
            () => {
              setLoading(false);
            },
            () => {
              setLoading(true);
            },
          );
        }}
        style={[styles.btnNext, loading ? styles.loading : {}]}>
        <Text>Finalizar Registro</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    borderRadius: 50,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: Colors.colorBlue,
  },
  loading: {
    backgroundColor: '#CCC',
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginBottom: 20,
    backgroundColor: Colors.colorBlue,
    borderRadius: 50,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
});

const mapStateToProps = state => ({
  generalData: state.register.generalData,
});

const mapDispatchToProps = dispatch => ({
  setInstituicao: instData =>
    dispatch({type: Actions.setInstituicao, payload: instData}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CadastroInstituicao);
