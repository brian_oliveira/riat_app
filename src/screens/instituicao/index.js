import React, {useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  StyleSheet,
  ImageBackground,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../assets/colors';
import {connect} from 'react-redux';
import {getPacientes, setMedicos, getSessoes} from '../../DAOs/instituicao';
import Actions from '../../redux/actions/actions';

const options = [
  {
    text: 'Solicitar',
    image: 'folder-plus',
    onPress: props => props.navigation.navigate('SolicitacaoAcompanhamento'),
  },
  {
    text: 'Pacientes',
    image: 'users',
    onPress: props => props.navigation.navigate('PacienteListagem'),
  },
  {
    text: 'Servicios',
    image: 'calendar-check',
    onPress: props => props.navigation.navigate('AcompanamientosInstituicao'),
  },
  {
    text: 'Administrativo',
    image: 'dollar-sign',
    onPress: props => props.navigation.navigate('AdministrativoInstituicao'),
  },
  {
    text: 'Funcionários',
    image: 'stethoscope',
    onPress: props => props.navigation.navigate('MedicoListagem'),
  },
  {
    text: 'Chat',
    image: 'comments',
    onPress: props => props.navigation.navigate('ChatRooms'),
  },
];

const InstituicaoIndex = props => {
  useEffect(() => {
    getSessoes(props);
    getPacientes(props);
    setMedicos(props);
  });

  return (
    <ImageBackground
      source={require('../../assets/splash_screen.png')}
      style={[{width: '100%', height: '100%'}]}
      imageStyle={{opacity: 0.3}}>
      <View>
        <Image
          style={[styles.logo, {marginTop: 38}]}
          source={require('../../assets/logo_riat.png')}
        />
        <Image
          style={[styles.logo, {height: Dimensions.get('window').height * 0.1}]}
          source={require('../../assets/logo_riat_nome.png')}
        />
        <View style={styles.row}>
          {options.map((option, index) => (
            <TouchableOpacity
              style={styles.iconOption}
              onPress={() => option.onPress(props)}>
              <Icon name={option.image} size={50} color={Colors.colorBlue} />
              <Text
                style={{
                  fontSize: 15,
                  textAlign: 'center',
                  marginTop: 10,
                  color: Colors.colorRed,
                }}>
                {option.text}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 30,
  },
  logo: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').height * 0.2,
    resizeMode: 'contain',
    margin: 0,
    alignSelf: 'center',
  },
  img: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  iconOption: {
    width: Dimensions.get('window').width * 0.33,
    padding: 10,
    alignItems: 'center',
  },
  icon: {
    fontSize: 30,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  setSessoesInstituicao: sessoes =>
    dispatch({type: Actions.setSessoesInstituicao, payload: sessoes}),
  setPacientes: pacientes =>
    dispatch({type: Actions.getPacientesInstituicao, payload: pacientes}),
  setMedicos: medicos => dispatch({type: Actions.setMedicos, payload: medicos}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InstituicaoIndex);
