/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../assets/colors';
import {Stitch, RemoteMongoClient} from 'mongodb-stitch-react-native-sdk';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import {sendMessage} from '../../DAOs/paciente';

const renderAccordingly = props => {
  if ('chat' in props.pacienteSelected) {
    if ('messages' in props.pacienteSelected.chat) {
      return props.pacienteSelected.chat.messages.map((message, index) => (
        <Message message={message.message} sender={message.sender} />
      ));
    } else {
      return <></>;
    }
  } else {
    return <></>;
  }
};

const Message = props => {
  return (
    <View style={styles.flexEnd}>
      <View>
        <Text style={[styles.messageContent]}>{props.message}</Text>
      </View>
      <Text style={styles.senderMessage}>{props.sender}</Text>
    </View>
  );
};

const Chat = props => {
  const [message, setMessage] = useState('');
  const [isSending, setSending] = useState(false);
  const [chat, setChatMessages] = useState([]);
  useEffect(() => {
    let pacientes = [];
    let pacienteSelected = {};

    if (
      props.pacienteSelected.chat.messages &&
      props.pacienteSelected.chat.messages.length > 0
    ) {
      setChatMessages([...props.pacienteSelected.chat.messages]);
    }

    // alert(JSON.stringify(props.pacienteSelected.chat.messages));
    switch (props.userLogged.userType) {
      case 'familiar':
        pacientes = [...props.pacientesFam];
        pacienteSelected = {...props.pacienteSelectedFam};
        break;
      case 'funcionario':
        pacientes = [...props.pacientesInst];
        pacienteSelected = {...props.pacienteSelectedInst};
        break;
      default:
        pacientes = [...props.pacientesGeral];
        pacienteSelected = {...props.pacienteSelected};
        break;
    }
  }, []);

  React.useEffect(() => {
    const streaming = async () => {
      const app = Stitch.defaultAppClient;
      const mongoDB = app.getServiceClient(
        RemoteMongoClient.factory,
        'mongodb-atlas',
      );
      const pacientesCollection = mongoDB.db('RIAT_DB').collection('pacientes');
      const stream = await pacientesCollection.watch();
      stream.onNext(event => {
        alert(JSON.stringify(event));
      });
    };
    streaming();
  });

  return (
    <ImageBackground
      source={require('../../assets/splash_screen.png')}
      style={[{width: '100%', height: '100%'}]}
      imageStyle={styles.lowOpacity}>
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.messagesContainer}>
          <FlatList
            data={chat}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item, index}) => (
              <Message message={item.message} sender={item.sender} />
            )}
          />
          {/*renderAccordingly(props)*/}
        </ScrollView>
        <View style={styles.sendMessageContainer}>
          <TextInput
            value={message}
            onChangeText={text => setMessage(text)}
            style={styles.sendMessageInput}
            placeholder={'Informe aqui sua mensagem...'}
          />
          <TouchableOpacity
            onPress={async () => {
              setSending(true);
              const res = await sendMessage({
                pacienteId: props.pacienteSelected._id,
                message: message,
                sender: props.userLogged.nombre,
              });
              res
                ? setChatMessages([
                    ...chat,
                    {message: message, sender: props.userLogged.nombre},
                  ])
                : null;
              setMessage('');
              setSending(false);
            }}
            style={styles.sendButton}>
            {isSending ? (
              <ActivityIndicator size="small" color="#fff" />
            ) : (
              <Icon name={'send'} size={25} color={'#FFFFFF'} />
            )}
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

Chat.navigationOptions = ({navigation}) => ({
  headerTitle: (
    <View>
      <Text style={{fontSize: 18}}>{navigation.getParam('title')}</Text>
      <Text style={{fontSize: 10, color: '#CCC'}}>3 participantes</Text>
    </View>
  ),
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sendMessageContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  flexEnd: {
    justifyContent: 'flex-end',
  },
  messagesContainer: {
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  sendMessageInput: {
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    backgroundColor: '#FFFFFF',
    elevation: 8,
    flex: 8.5,
  },
  senderMessage: {
    color: '#888',
    marginBottom: 10,
    marginLeft: 10,
  },
  lowOpacity: {
    opacity: 0.3,
  },
  sendButton: {
    flex: 1.5,
    backgroundColor: Colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  messageContent: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 11,
    paddingBottom: 11,
    backgroundColor: '#FFF',
    alignSelf: 'flex-start',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    marginLeft: 10,
    borderTopLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  pacientesInst: state.instituicao.pacientes,
  pacienteSelected: state.paciente.pacienteSelected,
  pacientesGeral: state.paciente.pacientes,
  pacientesFam: state.familiar.pacientes,
});

const mapDispatchToProps = dispatch => ({
  selectPacienteFamiliar: paciente =>
    dispatch({type: Actions.selectPacienteFamiliar, payload: paciente}),
  selectPacienteInstituicao: paciente =>
    dispatch({type: Actions.selectPacienteInstituicao, payload: paciente}),
  setPacientes: pacientes =>
    dispatch({type: Actions.getPacientesInstituicao, payload: pacientes}),
  getPacientesSuccess: pacientes =>
    dispatch({type: Actions.getPacientesSuccess, payload: pacientes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);
