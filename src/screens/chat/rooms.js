/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import Actions from '../../redux/actions/actions';
import {getPacientes} from '../../DAOs/instituicao';
import {getPacientes as getPacientesFam} from '../../DAOs/familiar';

const renderAccordingly = props => {};

const Rooms = props => {
  return (
    <ScrollView>
      {props.pacientes.map((paciente, index) => {
        if (paciente == null) {
          return <></>;
        } else {
          return (
            <TouchableOpacity
              key={index}
              style={styles.room}
              onPress={() => {
                props.setPacienteGeral(paciente);
                props.navigation.navigate('Chat', {
                  title:
                    'nombreCompleto' in paciente ? paciente.nombreCompleto : '',
                });
              }}>
              <Text style={styles.pacienteName}>
                {'nombreCompleto' in paciente ? paciente.nombreCompleto : ''}
              </Text>
              <Text style={styles.participantes}>3 participantes</Text>
            </TouchableOpacity>
          );
        }
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  room: {
    padding: 15,
    borderBottomWidth: 1,
    borderColor: '#CCC',
  },
  pacienteName: {
    fontSize: 18,
  },
  participantes: {
    color: '#999',
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  pacientes: state.paciente.pacientes,
});

const mapDispatchToProps = dispatch => ({
  setPacienteGeral: paciente =>
    dispatch({type: Actions.setPacienteGeral, payload: paciente}),
  setPacientesGeral: pacientes =>
    dispatch({type: Actions.setPacientesGeral, payload: pacientes}),
  selectPacienteFamiliar: paciente =>
    dispatch({type: Actions.selectPacienteFamiliar, payload: paciente}),
  selectPacienteInstituicao: paciente =>
    dispatch({type: Actions.selectPacienteInstituicao, payload: paciente}),
  setPacientes: pacientes =>
    dispatch({type: Actions.getPacientesInstituicao, payload: pacientes}),
  getPacientesSuccess: pacientes =>
    dispatch({type: Actions.getPacientesSuccess, payload: pacientes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Rooms);
