/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  ImageBackground,
  StyleSheet,
  Alert,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../assets/colors';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import {getPacientes} from '../../DAOs/familiar';
import axios from 'axios';
import {URL} from '../../assets/consts';

const options = [
  {
    text: 'Solicitar',
    image: 'folder-plus',
    onPress: props => props.navigation.navigate('SolicitacaoAcompanhamento'),
  },
  {
    text: 'Pacientes',
    image: 'users',
    onPress: props => props.navigation.navigate('PacienteListagem'),
  },
  {
    text: 'Administrativo',
    image: 'dollar-sign',
    onPress: props => props.navigation.navigate('AdministrativoFamiliar'),
  },
  //TODO: Histórico
  {
    text: 'Noticias',
    image: 'newspaper',
    onPress: props => props.navigation.navigate('Noticias'),
  },
  {
    text: 'Hable con Nosotros',
    image: 'comments',
    onPress: () =>
      Linking.openURL('https://api.whatsapp.com/send?phone=5491127048745'),
  },
];

const FamiliarMenu = props => {

  useEffect(() => getPacientes(props));

  return (
    <ImageBackground
      source={require('../../assets/splash_screen.png')}
      style={[{width: '100%', height: '100%'}]}
      imageStyle={{opacity: 0.3}}>
      <View>
        <Image
          style={[styles.logo, {marginTop: 38}]}
          source={require('../../assets/logo_riat.png')}
        />
        <Image
          style={[styles.logo, {height: Dimensions.get('window').height * 0.1}]}
          source={require('../../assets/logo_riat_nome.png')}
        />
        <View style={styles.row}>
          {options.map((option, index) => (
            <TouchableOpacity
              style={styles.iconOption}
              onPress={() => option.onPress(props)}>
              <Icon name={option.image} size={50} color={Colors.colorBlue} />
              <Text
                style={{
                  fontSize: 15,
                  textAlign: 'center',
                  marginTop: 10,
                  color: Colors.colorRed,
                }}>
                {option.text}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginTop: 30,
  },
  logo: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').height * 0.2,
    resizeMode: 'contain',
    margin: 0,
    alignSelf: 'center',
  },
  img: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  iconOption: {
    width: Dimensions.get('window').width * 0.33,
    padding: 10,
    alignItems: 'center',
  },
  icon: {
    fontSize: 30,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  getPacientesSuccess: pacientes =>
    dispatch({type: Actions.getPacientesSuccess, payload: pacientes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FamiliarMenu);
