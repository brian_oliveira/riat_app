import React, {useState} from 'react';
import {Picker, ScrollView, Text, Image, View, StyleSheet} from 'react-native';
import {
  calcularHorasTrabalhadas,
  getValorTotal,
  returnHorasTrabalhadas,
} from '../../../assets/utils';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../assets/colors';
import {connect} from 'react-redux';
import moment from 'moment/min/moment-with-locales';

const getArrayMeses = () => {
  let arrayMonths = [];
  let actualMonth = '';
  while (!moment(new Date()).isSame(actualMonth, 'month')) {
    actualMonth = moment('2019-11-01').add(1, 'month');
    arrayMonths.push(actualMonth.locale('esR').format('MMMM/YYYY'));
  }
  return arrayMonths;
};

const TrabajoComponent = props => {
  //TODO: Valor da hora

  return (
    <View style={styles.containerTrabajo}>
      <Text>Atendido por: {props.sessao.acompanhante.nombre}</Text>
      <Text>
        Horas Trabajadas:{' '}
        {calcularHorasTrabalhadas(
          props.sessao.horaInicio,
          props.sessao.horarioFim,
        ) + '\n'}
        Valor Neto de la hora: {150 + '\n'}
        Monto Total: $
        {returnHorasTrabalhadas(
          props.sessao.horaInicio,
          props.sessao.horarioFim,
        ) * parseFloat(props.userLogged.valorHora)}
      </Text>
      {/*<Text>Estimativa de Pago: 21/05/2019</Text>*/}
    </View>
  );
};

const AdministrativoFamiliar = props => {
  // TODO: FAZER UM MAPBOX COM TODOS OS CHECK IN E CHECK OUT

  const getAllSessoes = () => {
    let sessoes = [];

    props.pacientes.forEach(paciente => {
      sessoes.push(paciente.sessao);
    });

    return sessoes;
  };

  console.log(props);
  return (
    <ScrollView style={styles.container}>
      <Picker style={{marginTop: 15}}>
        {getArrayMeses().map(month => (
          <Picker.Item label={month} value={month}/>
        ))}
      </Picker>
      <LinearGradient
        style={styles.header}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[Colors.colorBlue, Colors.colorLightBlue]}>
        <Text style={[styles.headerText, {fontSize: 15}]}>A Pagar:</Text>
        <Text style={[styles.headerText, {fontSize: 45}]}>
          ${/*getValorTotal(getAllSessoes(), 150)*/}
        </Text>
        <Text style={[styles.headerText]}>Horas trabajadas: 300h</Text>
      </LinearGradient>
      <Text
        style={{
          textTransform: 'uppercase',
          color: '#AAA',
          marginTop: 15,
          marginBottom: 10,
          marginLeft: 15,
          fontSize: 20,
        }}>
        Detalles
      </Text>
      {props.pacientes.map(paciente => (
        <View>
          <Text>{paciente.nombreCompleto}</Text>
          {paciente.sessoes ? (
            paciente.sessoes.map(sessao => <TrabajoComponent sessao={sessao} />)
          ) : (
            <Text style={{color: 'red'}}>Sessões ainda não disponíveis</Text>
          )}
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  header: {
    justifyContent: 'center',
    marginTop: 20,
    shadowColor: '#000',
    marginLeft: 15,
    marginRight: 15,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    padding: 15,
    borderRadius: 10,
  },
  headerText: {
    textTransform: 'uppercase',
    textAlign: 'center',
    color: '#FFFFFF',
  },
  containerTrabajo: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    borderRadius: 5,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    padding: 10,
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
  },
});

// const AdministrativoFamiliar = props => {
//     // TODO: Identica a Instituicao
//   return (
//     <ScrollView style={styles.container}>
//       <Image
//         style={styles.patientThumbnail}
//         source={{
//           uri:
//             'https://images.unsplash.com/photo-1548365108-908055adb1c8?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
//         }}
//       />
//       <Text
//         style={{fontSize: 17, textTransform: 'uppercase', alignSelf: 'center'}}>
//         Augustina Lopez
//       </Text>
//       <Text style={{fontSize: 14, alignSelf: 'center'}}>
//         5 sesiones hasta ahora
//       </Text>
//       <Picker>
//         <Picker.Item label="Augustina Lopez" value="Augustina Lopez" />
//         <Picker.Item label="Leandro Schmidt" value="Leandro Schmidt" />
//       </Picker>
//       <View style={styles.row}>
//         <Text>Total a Pagar:</Text>
//         <Text style={{flex: 1, textAlign: 'right'}}>$4.500,00</Text>
//       </View>
//       <View style={styles.row}>
//         <Text>Horas Recibidas:</Text>
//         <Text style={{flex: 1, textAlign: 'right'}}>135H</Text>
//       </View>
//       <View style={styles.row}>
//         <Text>Valor por Hora:</Text>
//         <Text style={{flex: 1, textAlign: 'right'}}>$150,00</Text>
//       </View>
//     </ScrollView>
//   );
// };
//
// const styles = StyleSheet.create({
//   container: {
//     paddingLeft: 15,
//     paddingRight: 15,
//     flex: 1,
//   },
//   row: {
//     flexDirection: 'row',
//     paddingTop: 20,
//   },
//   patientThumbnail: {
//     marginTop: 30,
//     marginBottom: 15,
//     width: 80,
//     alignSelf: 'center',
//     height: 80,
//     shadowOffset: {
//       width: 0,
//       height: 7,
//     },
//     shadowOpacity: 0.43,
//     shadowRadius: 9.51,
//     elevation: 8,
//     shadowColor: '#000',
//     borderRadius: 40,
//     backgroundColor: '#eee',
//   },
// });

const mapStateToProps = state => ({
  pacientes: state.familiar.pacientes,
});

export default connect(mapStateToProps)(AdministrativoFamiliar);
