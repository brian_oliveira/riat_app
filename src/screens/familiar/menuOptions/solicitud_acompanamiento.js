/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import Modal from 'react-native-modalbox';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Picker,
  Alert,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import Colors from '../../../assets/colors';
import {URL, FORMA_PAGO, DIAS_SEMANA} from '../../../assets/consts';
import axios from 'axios';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-check-box';
import {TextInputMask} from 'react-native-masked-text';
import {connect} from 'react-redux';
import Actions from '../../../redux/actions/actions';
import tipoProfissional from '../../profissional/cadastro/tipoProfissional';

const radioType = [
  {label: 'Normal', value: 'Normal'},
  {label: 'Emergencia', value: 'Emergencia'},
];

const SolicitudAcompanamiento = props => {
  const [solicitacao, setSolicitacao] = useState({
    tipoProfissional: 'at',
    selectedPaciente: '',
    diagnostico: '',
    fechaDeInicio: '',
    fechaDeFin: '',
    horarioInicio: '',
    horarioFin: '',
    freq: [],
    formaDelPago: FORMA_PAGO[0],
    tipoInternacao: 'Normal',
    load: false,
    pacientes: null,
    previsao: true,
  });

  const [diaSemanaAux, setDiaSemana] = useState('Lunes');
  const [hrs24, set24hrs] = useState(false);
  const [pacienteSelected, selectPaciente] = useState({});

  const checkIfFamiliarOrInstituicao = () => {
    let pacientes = [];

    if (props.userLogged.userType === 'familiar') {
      props.pacientesFam.forEach(paciente => {
        if (paciente.solicitacao.length <= 0) {
          pacientes.push(paciente);
        }
      });
    } else if (props.userLogged.userType === 'funcionario') {
      props.pacientesInst.forEach(paciente => {
        if (paciente.solicitacao.length <= 0) {
          pacientes.push(paciente);
        }
      });
    }
    return pacientes;
  };

  const sendDataToServer = data => {
    setSolicitacao({...solicitacao, load: true});
    axios
      .post(URL + 'solicitacao/incoming_webhook/createSolicitacao', data)
      .then(data => {
        Alert.alert('Exito!', 'Solicitud realizada con éxito!');
        setSolicitacao({...solicitacao, load: false});
        props.navigation.navigate('FamiliarMenu');
      })
      .catch(err => {
        Alert.alert('Perdón!', 'Aconteció un error... ' + err);
        setSolicitacao({...solicitacao, load: false});
      });
  };

  const [modalShow, showModal] = useState(false);

  const diaSemanaOptions = [
    ...DIAS_SEMANA,
    'Todos los Días',
    'Lunes a Viernes',
  ];

  return (
    <View style={styles.fullContainer}>
      <Modal
        isOpen={modalShow}
        onClosed={() => showModal(false)}
        style={styles.modalContainer}
        startOpen={true}
        coverScreen={true}
        position={'bottom'}>
        <Text>Dia da semana</Text>
        <Picker
          selectedValue={diaSemanaAux}
          onValueChange={value => setDiaSemana(value)}>
          {diaSemanaOptions.map(dia => (
            <Picker.Item label={dia} value={dia} />
          ))}
        </Picker>
        <Text>Horário de Início</Text>
        <TextInputMask
          type={'datetime'}
          disabled={hrs24}
          options={{
            format: 'HH:mm',
          }}
          value={hrs24 ? '24 horas' : solicitacao.horarioInicio}
          onChangeText={text =>
            setSolicitacao({...solicitacao, horarioInicio: text})
          }
        />
        <Text>Horário del Fín</Text>
        <TextInputMask
          type={'datetime'}
          options={{
            format: 'HH:mm',
          }}
          disabled={hrs24}
          value={hrs24 ? '24 horas' : solicitacao.horarioFin}
          onChangeText={text =>
            setSolicitacao({...solicitacao, horarioFin: text})
          }
        />
        <CheckBox
          label={'No tengo prevision.'}
          style={{marginBottom: 10, marginTop: 10}}
          rightText={'No tengo ningún psicologo o psiquiatra.'}
          onClick={() => {
            set24hrs(!hrs24);
          }}
          isChecked={hrs24}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            let freqAux = [...solicitacao.freq];
            freqAux.push({
              dia: diaSemanaAux,
              horaInicio: hrs24 ? '24 Horas' : solicitacao.horarioInicio,
              horaFin: hrs24 ? '24 Horas' : solicitacao.horarioFin,
            });

            let newSolicitacao = {...solicitacao};
            delete newSolicitacao.horarioInicio;
            delete newSolicitacao.horarioFin;

            setSolicitacao({
              ...newSolicitacao,
              freq: freqAux,
            });
            showModal(false);
          }}>
          <Text style={styles.textButton}>Añadir</Text>
        </TouchableOpacity>
      </Modal>
      <ScrollView style={styles.container}>
        <Text style={[{marginTop: 20}, styles.textLabel]}>
          Elija un Profesional
        </Text>
        <Picker
          selectedValue={solicitacao.tipoProfissional}
          onValueChange={itemValue =>
            setSolicitacao({...solicitacao, tipoProfissional: itemValue})
          }>
          <Picker.Item label="Acompañante Terepéutico" value="at" />
          <Picker.Item label="Operador Socio-Terapéutico" value="ost" />
          <Picker.Item label="Cuidador" value="cui" />
        </Picker>
        <Text style={styles.textLabel}>Elija un Paciente</Text>
        <Picker
          selectedValue={solicitacao.selectedPaciente}
          onValueChange={itemValue => {
            setSolicitacao({...solicitacao, selectedPaciente: itemValue});
          }}>
          <Picker.Item label="" value="null" />
          {checkIfFamiliarOrInstituicao().map(item => (
            <Picker.Item
              key={item._id.$oid}
              label={item.nombreCompleto}
              value={item}
            />
          ))}
        </Picker>
        <Text style={styles.textLabel}>Clase de Tratamiento</Text>
        <RadioForm
          radio_props={radioType}
          initial={0}
          onPress={value => {
            setSolicitacao({...solicitacao, tipoInternacao: value});
          }}
        />
        <Text style={styles.textLabel}>Fecha de Inicio</Text>

        <DatePicker
          style={{width: Dimensions.get('window').width - 30}}
          format={'DD/MM/YYYY'}
          customStyles={{
            dateInput: [styles.textInput, {paddingTop: 20, paddingBottom: 20}],
            dateIcon: {display: 'none'},
          }}
          date={solicitacao.fechaDeInicio}
          onDateChange={text =>
            setSolicitacao({...solicitacao, fechaDeInicio: text})
          }
          placeholder={'Ingrese aquí la fecha de inicio'}
        />
        {solicitacao.previsao && (
          <View>
            <Text style={styles.textLabel}>Hasta cuando?</Text>
            <DatePicker
              date={solicitacao.fechaDeFin}
              format={'DD/MM/YYYY'}
              style={{width: Dimensions.get('window').width - 30}}
              customStyles={{
                dateInput: [
                  styles.textInput,
                  {paddingTop: 20, paddingBottom: 20},
                ],
                dateIcon: {display: 'none'},
              }}
              onDateChange={text =>
                setSolicitacao({...solicitacao, fechaDeFin: text})
              }
              placeholder={'Ingrese aquí la fecha de inicio'}
            />
          </View>
        )}

        <CheckBox
          label={'No tengo prevision.'}
          style={{marginBottom: 10, marginTop: 10}}
          rightText={'No tengo prevision del fín.'}
          onClick={() => {
            setSolicitacao({
              ...solicitacao,
              previsao: !solicitacao.previsao,
              fechaDeFin: 'Inderterminado',
            });
          }}
          isChecked={!solicitacao.previsao}
        />

        <Text style={styles.textLabel}>Frecuencia de Servicio</Text>
        {solicitacao.freq.map(freq => (
          <Text>
            {freq.dia} {'\n'}
            {freq.horaInicio} ~ {freq.horaFin}
          </Text>
        ))}
        {solicitacao.tipoInternacao === 'Emergencia' ? (
          <Text>Todos los días, 24hs, por 72hs.</Text>
        ) : (
          <TouchableOpacity
            style={styles.button}
            onPress={() => showModal(true)}>
            <Text style={styles.textButton}>Añadir</Text>
          </TouchableOpacity>
        )}
        <Text style={styles.textLabel}>Forma del Pago</Text>
        <Picker
          selectedValue={solicitacao.formaDelPago}
          onValueChange={itemValue =>
            setSolicitacao({...solicitacao, formaDelPago: itemValue})
          }>
          {FORMA_PAGO.map(forma => (
            <Picker.Item label={forma} value={forma} />
          ))}
        </Picker>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            let sol = {
              ...solicitacao,
              diagnostico: solicitacao.selectedPaciente.diagnostico,
              selectedPaciente: solicitacao.selectedPaciente._id,
            };

            if (solicitacao.tipoInternacao === 'Emergencia') {
              sol.freq = [
                {
                  dia: 'Todos los días',
                  horaInicio: '24hs',
                  horaFin: '24hs',
                },
              ];
            }

            delete sol.load;
            delete sol.pacientes;

            sendDataToServer(sol);
          }}>
          {solicitacao.load ? (
            <ActivityIndicator size="small" />
          ) : (
            <Text style={styles.textButton}>Solicitación</Text>
          )}
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    height: 300,
    zIndex: 10001,
    padding: 15,
    backgroundColor: 'white',
  },
  fullContainer: {
    flex: 1,
  },
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
  },
  button: {
    flex: 1,
    margin: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    backgroundColor: Colors.colorBlue,
    padding: 10,
    borderRadius: 5,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textButton: {
    color: '#FFF',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 50,
    marginTop: 5,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(98,100,207,1)',
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  pacientesInst: state.instituicao.pacientes,
  pacientesFam: state.familiar.pacientes,
});

const mapDispatchToProps = dispatch => ({
  setPacientes: pacientes =>
    dispatch({type: Actions.getPacientesInstituicao, payload: pacientes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SolicitudAcompanamiento);
