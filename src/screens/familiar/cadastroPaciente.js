import React, {useState} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  Alert,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {URL} from '../../assets/consts';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import axios from 'axios';

const CadastroPaciente = props => {
  const [pacienteData, setData] = useState({
    nombreCompleto: '',
    fechaDeNacimiento: '',
    direccion: '',
    DNI: '',
    diagnostico: '',
    psiquiatra: '',
    telefonoPsiquiatra: '',
    psicologo: '',
    telefonoPsicologo: '',
  });

  const sendDataToServer = data => {
    axios
      .post(URL + 'familiar/incoming_webhook/createFamiliar', data)
      .then(data => {
        Alert.alert('Bienvenido a RIAT!', 'Usted completó su registro!');
      })
      .catch(err => {
        Alert.alert('Perdón!', 'Aconteció un error... ' + err);
      });
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textLabel}>Nombre Completo</Text>
      <TextInput
        onChangeText={text => setData({...pacienteData, nombreCompleto: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: Carlos Contreras'}
      />
      <Text style={styles.textLabel}>Fecha de Nacimiento</Text>
      <TextInput style={styles.textInput} />
      <Text style={styles.textLabel}>Dirección</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={text => setData({...pacienteData, direccion: text})}
        placeholder={'Ejemplo: Calle 10, 32'}
      />
      <Text style={styles.textLabel}>DNI</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={text => setData({...pacienteData, DNI: text})}
        placeholder={'Ejemplo: 9999999999'}
      />
      <Text style={styles.textLabel}>Diagnostico</Text>
      <TextInput
        onChangeText={text => setData({...pacienteData, diagnostico: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: depresión aguda y ansiedad'}
      />
      <Text style={styles.textLabel}>Psiquiatra</Text>
      <TextInput
        onChangeText={text => setData({...pacienteData, psiquiatra: text})}
        style={styles.textInput}
        placeholder={'Informe el nombre del médico.'}
      />
      <Text style={styles.textLabel}>Teléfono del Psiquiatra</Text>
      <TextInput
        onChangeText={text =>
          setData({...pacienteData, telefonoPsiquiatra: text})
        }
        style={styles.textInput}
        placeholder={'Ejemplo: 11 2345 6789'}
      />
      <Text style={styles.textLabel}>Psicologo</Text>
      <TextInput
        onChangeText={text => setData({...pacienteData, psicologo: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: Carlos Andrade'}
      />
      <Text style={styles.textLabel}>Teléfono del Psicológo</Text>
      <TextInput
        onChangeText={text =>
          setData({...pacienteData, telefonoPsicologo: text})
        }
        placeholder={'Ejemplo: 11 2345 6789'}
        style={styles.textInput}
      />
      <TouchableOpacity
        style={styles.btnNext}
        onPress={() => {
          sendDataToServer({
            ...props.generalData,
            usuario: {
              email: props.generalData.email,
              password: props.generalData.contrasena,
              type: 'familiar',
            },
            paciente: pacienteData,
          });
          props.setPaciente(pacienteData);
        }}>
        <Text style={styles.uppercaseAndWhite}>Finalizar Registro</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  uppercaseAndWhite: {
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    borderRadius: 50,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(98,100,207,1)',
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 15,
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: 'rgba(98,100,207,1)',
    borderRadius: 50,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
});

const mapStateToProps = state => ({
  generalData: state.register.generalData,
});

const mapDispatchToProps = dispatch => ({
  setPaciente: data => dispatch({type: Actions.setPaciente, payload: data}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CadastroPaciente);
