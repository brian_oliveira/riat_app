import React from 'react';
import { View, ScrollView, Text, StyleSheet, Image} from 'react-native';
import Colors from '../../../assets/colors';

export default function menuOptions() {
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Psicologia Clinica Brasil</Text>
      <Image style={styles.image} source={{ uri: 'https://cdn.pixabay.com/photo/2018/12/04/22/38/road-3856796__340.jpg' }}/>
      <View style>
        <Text style={styles.textContainer}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed
          egestas condimentum. Integer libero ligula, sollicitudin ut fringilla
          fringilla ac, pulvinar vitae libero. Quisque vel ligula ante. Quisque
          non elit tempor, tincidunt elit non, volutpat lorem. Nam cursus turpis
          a nulla consequat, finibus commodo felis volutpat. Quisque at dui est.
          Integer eu pretium tortor. Maecenas bibendum hendrerit quam eget
          maximus. Quisque eu arcu in tellus malesuada viverra et eu ex. Nunc
          vehicula aliquam ante non lobortis. Suspendisse dui arcu, mollis at
          pellentesque vel, placerat sit amet turpis. Aliquam convallis tellus
          et fringilla sagittis. pellentesque vel, placerat sit amet turpis. 
          Aliquam convallis tellus et fringilla sagittis.
          Maecenas bibendum hendrerit quam eget
          maximus. Quisque eu arcu in tellus malesuada viverra et eu ex. Nunc
          vehicula aliquam ante non lobortis. Suspendisse dui arcu, mollis at
          pellentesque vel, placerat sit amet turpis. Aliquam convallis tellus
          et fringilla sagittis. pellentesque vel, placerat sit amet turpis. 
          Aliquam convallis tellus et fringilla sagittis.
        </Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 10
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: Colors.colorRed,
    marginVertical: 15,
  },
  image: {
    width: 300,
    height: 200,
    borderRadius: 4,
    resizeMode: 'contain',
    // backgroundColor: '#eee',
    marginBottom: 15,
  },
  textContainer: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#eee',
    padding: 5,
  },
  text: {
    fontSize: 14,
    textAlign: 'center',
  },
});
