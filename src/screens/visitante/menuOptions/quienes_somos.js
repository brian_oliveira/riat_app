import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Dimensions,
  StyleSheet,
} from 'react-native';
import Colors from '../../../assets/colors';

const QuienesSomos = props => {
  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{
        alignItems: 'center',
      }}>
      <Image
        style={[styles.logo, {marginTop: 30}]}
        source={require('../../../assets/logo_riat.png')}
      />
      <Image
        style={[styles.logo, {height: Dimensions.get('window').height * 0.1}]}
        source={require('../../../assets/logo_riat_nome.png')}
      />
      <Text style={{color: Colors.colorRed, fontSize: 18, marginTop: 25}}>
        Quienes Somos
      </Text>
      <Text
        style={{color: Colors.colorBlue, textAlign: 'center', marginTop: 17}}>
        Es una red de acompañamiento terapéutico creada en 2017 con el propósito
        de organizar la relación entre diversos acompañantes terapéuticos, sus
        pacientes / familiares, equipo tratante (psicólogos y psiquiatras) y las
        instituciones asociadas, utilizando herramientas contemporáneas de
        tecnología de la información para permitir un ajuste dinamico entre las
        demandas del paciente con los objetivos terapéuticos, la elaboración de
        estrategias y tácticas terapéuticas adecuadas a ese fin, permitiendo un
        aumento en la integración del equipo, con la realización de
        informes diarios de cada atendimiento realizado, con la adecuada
        supervisión y soporte a los profesionales que están acompañando, para
        que esa fortaleza y cuidado generado dentro del equipo sea transmitido a
        los pacientes , aumentando la eficiencia del tratamiento y un mejor
        resultado terapéutico, como consecuencia de una mayor precisión en las
        intervenciones, oriunda de una transparencia de la información
        compartida instantáneamente entre el equipo.
      </Text>
      <Text style={{marginTop: 20}}>Contactenos</Text>
      <Text>Laprida, 1950</Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    paddingLeft: 15,
    borderRadius: 50,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: 'rgba(121,130,227,1)',
  },
  logo: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').height * 0.2,
    resizeMode: 'contain',
    margin: 0,
    alignSelf: 'center',
  },
  whiteFont: {
    color: '#FFF',
  },
  buttonEntrar: {
    marginTop: 15,
    borderRadius: 50,
    padding: 13,
    alignItems: 'center',
    backgroundColor: 'rgba(98,100,207,1)',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 3,
  },
  center: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
});

export default QuienesSomos;
