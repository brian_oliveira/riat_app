import React from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../../assets/colors';

const Noticia = props => {
  return (
    <TouchableOpacity style={styles.cardNoticia} onPress={() => props.navigation.navigate('NoticiaPage')}>
      <View style={styles.row}>
        <Image
          style={{width: 80, height: 100, resizeMode: 'contain', marginRight: 10}}
          source={require('../../../assets/marker.png')}
        />
        <View style={{flex: 2}}>
          <Text style={{color: Colors.colorRed}}>Psicologia Clínica</Text>
          <Text numberOfLines={5}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            mattis lectus sed egestas condimentum. Integer libero ligula,
            sollicitudin ut fringilla ac, pulvinar vitae libero. Quisque vel
            ligula ante. Quisque non elit tempor, tincidunt elit non, volutpat
            lorem. Nam cursus turpis a nulla consequat, finibus commodo felis
            volutpat. Quisque at dui est. Integer eu pretium tortor. Maecenas
            bibendum hendrerit quam eget maximus. Quisque eu arcu in tellus
            malesuada viverra et eu ex. Nunc vehicula aliquam ante non lobortis.
            Suspendisse dui arcu, mollis at pellentesque vel, placerat sit amet
            turpis. Aliquam convallis tellus et fringilla sagittis.
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Noticias = props => {
  return (
    <ScrollView style={styles.container}>
      <Noticia navigation={props.navigation} />
      <Noticia navigation={props.navigation} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
  },
  cardNoticia: {
    backgroundColor: '#FFF',
    shadowColor: '#000',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 15,
    borderRadius: 7,
    padding: 10,
    marginBottom: 8,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
  },
  row: {
    flexDirection: 'row',
  },
});

export default Noticias;
