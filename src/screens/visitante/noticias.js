import React from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';

const NoticiasPage = () => {
  return (
    <View>
      {options.map((option, index) => (
        <TouchableOpacity onPress={option.onPress}>
          <Image source={option.image} />
          <Text>{option.text}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};
