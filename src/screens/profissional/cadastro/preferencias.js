import React, {useState} from 'react';
import CheckBox from 'react-native-check-box';
import {
  Alert,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Actions from '../../../redux/actions/actions';
import {connect} from 'react-redux';
import Colors from '../../../assets/colors';

const PreferenciasCadastroProfissional = props => {
  const [preferencias, setPreferencias] = useState({
    zona: '',
    disponibilidad: {
      manana: false,
      tarde: false,
      noche: false,
    },
  });

  const validateForm = form => {
    if (
      form.zona.length > 0 &&
      (form.disponibilidad.manana ||
        form.disponibilidad.tarde ||
        form.disponibilidad.noche)
    ) {
      console.log('tudo certo');
      return true;
    } else {
      console.log('faltando dados');
      return false;
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textLabel}>Zona de Trabajo</Text>
      <TextInput
        style={styles.textInput}
        placeholder={'Ejemplo: Buenos Aires, Capital Federal'}
        onChangeText={text => setPreferencias({...preferencias, zona: text})}
      />
      <Text style={styles.textLabel}>Disponibilidad para Trabajo</Text>
      <CheckBox
        onClick={() =>
          setPreferencias({
            ...preferencias,
            disponibilidad: {
              ...preferencias.disponibilidad,
              manana: !preferencias.disponibilidad.manana,
            },
          })
        }
        rightText={'Mañana'}
        isChecked={preferencias.disponibilidad.manana}
      />
      <CheckBox
        style={{marginTop: 10}}
        onClick={() =>
          setPreferencias({
            ...preferencias,
            disponibilidad: {
              ...preferencias.disponibilidad,
              tarde: !preferencias.disponibilidad.tarde,
            },
          })
        }
        rightText={'Tarde'}
        isChecked={preferencias.disponibilidad.tarde}
      />
      <CheckBox
        style={{marginTop: 10}}
        onClick={() =>
          setPreferencias({
            ...preferencias,
            disponibilidad: {
              ...preferencias.disponibilidad,
              noche: !preferencias.disponibilidad.noche,
            },
          })
        }
        rightText={'Noche'}
        isChecked={preferencias.disponibilidad.noche}
      />
      <TouchableOpacity
        onPress={() => {
          if (validateForm(preferencias)) {
            props.setPreferencias(preferencias);
            props.navigation.navigate('DocumentosProfesional');
          } else {
            Alert.alert(
              'Completa todos los campos',
              'Debes completar todos los campos.',
            );
          }
        }}
        style={styles.btnNext}>
        <Text style={styles.whiteFont}>ENVIAR</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 50,
    marginTop: 5,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: Colors.colorBlue,
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginBottom: 20,
    backgroundColor: 'rgba(98,100,207,1)',
    borderRadius: 50,
  },
  whiteFont: {
    textTransform: 'uppercase',
    color: '#FFF',
  },
});

const mapDispatchToProps = dispatch => ({
  setPreferencias: preferencias =>
    dispatch({
      type: Actions.setProfissionalPreferencias,
      payload: preferencias,
    }),
});

export default connect(
  null,
  mapDispatchToProps,
)(PreferenciasCadastroProfissional);
