import React, {useState} from 'react';
import {
  Dimensions,
  Picker,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../../assets/colors';
import Actions from '../../../redux/actions/actions';
import {connect} from 'react-redux';
import CheckBox from 'react-native-check-box';
import {
  Stitch,
  UserPasswordAuthProviderClient,
} from 'mongodb-stitch-react-native-core';
import {TIPO_PROFISSIONAL} from '../../../assets/consts';
import {newMedico} from '../../../DAOs/instituicao';

const TipoProfissional = props => {
  const [tipos, setTipos] = useState(
    new Array(TIPO_PROFISSIONAL.length).fill(false),
  );
  const [otroChecked, setOtroChecked] = useState(false);
  const [medicoData, setMedicoData] = useState({
    matriculaNacional: '',
    especialidad: '',
    tipoMedico: '',
  });
  return (
    <View style={styles.container}>
      {TIPO_PROFISSIONAL.map((tipo, index) => (
        <CheckBox
          rightText={tipo}
          isChecked={tipos[index]}
          onClick={() => {
            let aux = [...tipos];
            aux[index] = !aux[index];
            setTipos(aux);
          }}
        />
      ))}
      <CheckBox
        isChecked={otroChecked}
        onClick={() => setOtroChecked(!otroChecked)}
        rightText={'Otro...'}
      />
      {otroChecked ? (
        <View>
          <Text style={styles.textLabel}>Cuál?</Text>
          <TextInput
            onChangeText={text =>
              setMedicoData({...medicoData, tipoMedico: text})
            }
            style={styles.textInput}
            placeholder={'Ejemplo: Psicosocial'}
          />
        </View>
      ) : (
        <></>
      )}
      {tipos[3] || tipos[4] || tipos[5] || tipos[6] || tipos[7] ? (
        <View>
          <Text style={styles.textLabel}>Número de Matrícula</Text>
          <TextInput
            onChangeText={text =>
              setMedicoData({...medicoData, matriculaNacional: text})
            }
            style={styles.textInput}
          />
          <Text style={styles.textLabel}>Especialidad</Text>
          <TextInput
            onChangeText={text =>
              setMedicoData({...medicoData, especialidad: text})
            }
            style={styles.textInput}
          />
        </View>
      ) : (
        <></>
      )}
      <TouchableOpacity
        onPress={() => {
          if (tipos[3] || tipos[4] || tipos[5] || tipos[6] || tipos[7]) {
            newMedico(
              {
                ...props.generalData,
                matriculaNacional: medicoData.matriculaNacional,
                especialidad: medicoData.especialidad,
                tipoMedico: tipos,
                otro: medicoData.tipoMedico,
              },
              props,
              () => {
                const emailPassClient = Stitch.defaultAppClient.auth.getProviderClient(
                  UserPasswordAuthProviderClient.factory,
                );

                emailPassClient
                  .registerWithEmail(
                    props.generalData.email,
                    props.generalData.contrasena,
                  )
                  .then(res => {
                    alert('Listo! Bienvenido a RIAT!');
                  })
                  .catch(err => {
                    alert('erro: ' + err);
                  });
              },
            );
            props.navigation.navigate('Home');
          } else {
            props.setProfissionalTypes(tipos);
            props.navigation.navigate('ExperienciaProfesional');
          }
        }}
        style={styles.btnNext}>
        <Text style={styles.whiteFont}>Confirmar</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 50,
    marginTop: 5,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: Colors.colorBlue,
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginBottom: 20,
    backgroundColor: Colors.colorBlue,
    borderRadius: 50,
  },
  whiteFont: {
    textTransform: 'uppercase',
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  generalData: state.register.generalData,
});

const mapDispatchToProps = dispatch => ({
  setProfissionalTypes: types =>
    dispatch({type: Actions.setProfissionalTypes, payload: types}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TipoProfissional);
