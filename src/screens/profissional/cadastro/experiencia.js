import React, {useState} from 'react';
import DatePicker from 'react-native-datepicker';
import {
  Alert,
  Dimensions,
  View,
  Text,
  TextInput,
  Picker,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Actions from '../../../redux/actions/actions';
import {connect} from 'react-redux';
import Colors from '../../../assets/colors';

const Experiencia = props => {
  const [exp, setExp] = useState({
    descripcion: '',
    experiencia: 'Acompañante Terapéutico',
    fechaInicio: new Date(),
    fechaFin: new Date(),
    local: '',
  });

  const validateForm = form => {
    if (
      form.descripcion.length > 0 &&
      form.experiencia.length &&
      form.local.length > 0
    ) {
      console.log('tudo certo');
      return true;
    } else {
      console.log('faltando dados');
      return false;
    }
  };

  return (
    <View style={styles.container}>
      {/* Retirado porque a escolha já é feita na tela anterior. */}
      {/*<Picker*/}
      {/*  selectedValue={exp.experiencia}*/}
      {/*  onValueChange={itemValue => setExp({...exp, experiencia: itemValue})}>*/}
      {/*  <Picker.Item*/}
      {/*    label={'Acompañante Terapéutico'}*/}
      {/*    value={'Acompañante Terapéutico'}*/}
      {/*  />*/}
      {/*  <Picker.Item label={'Cuidador'} value={'Cuidador'} />*/}
      {/*  <Picker.Item*/}
      {/*    label={'Operador Sócio-Terapéutico'}*/}
      {/*    value={'Operador Sócio-Terapéutico'}*/}
      {/*  />*/}
      {/*  <Picker.Item label={'Psicanalista'} value={'Psicanalista'} />*/}
      {/*  <Picker.Item label={'Psicólogo'} value={'Psicólogo'} />*/}
      {/*  <Picker.Item label={'Psiquiatra'} value={'Psiquiatra'} />*/}
      {/*  <Picker.Item label={'Otro...'} value={'Otro...'} />*/}
      {/*</Picker>*/}
      <Text style={styles.textLabel}>Descripción</Text>
      <TextInput
        multiline={true}
        onChangeText={text => setExp({...exp, descripcion: text})}
        style={[styles.textInput, {height: 100, borderRadius: 10}]}
        placeholder={'Describa aquí su experiencia labural'}
      />
      <Text style={styles.textLabel}>Fecha de inicio</Text>
      <DatePicker
        style={{width: Dimensions.get('window').width - 30}}
        format={'DD/MM/YYYY'}
        customStyles={{
          dateInput: [styles.textInput, {paddingTop: 20, paddingBottom: 20}],
          dateIcon: {display: 'none'},
        }}
        onDateChange={date => setExp({...exp, fechaInicio: date})}
        date={exp.fechaInicio}
        placeholder={'Describa aquí su experiencia labural'}
      />
      <Text style={styles.textLabel}>Fecha del fín</Text>
      <DatePicker
        style={{width: Dimensions.get('window').width - 30}}
        format={'DD/MM/YYYY'}
        customStyles={{
          dateInput: [styles.textInput, {paddingTop: 20, paddingBottom: 20}],
          dateIcon: {display: 'none'},
        }}
        date={exp.fechaFin}
        onDateChange={date => setExp({...exp, fechaFin: date})}
        placeholder={'Describa aquí su experiencia labural'}
      />
      <Text style={styles.textLabel}>Local</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={text => setExp({...exp, local: text})}
        placeholder={'Ejemplo: Capital Federal, Bs. As.'}
      />
      <TouchableOpacity
        onPress={() => {
          if (validateForm(exp)) {
            props.setExperiencia(exp);
            props.navigation.navigate('PreferenciasProfesional');
          } else {
            Alert.alert(
              'Completa todos los campos',
              'Debes completar todos los campos.',
            );
          }
        }}
        style={styles.btnNext}>
        <Text style={{color: '#FFFFFF'}}>ENVIAR</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 50,
    marginTop: 5,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: Colors.colorBlue,
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginBottom: 20,
    backgroundColor: Colors.colorBlue,
    borderRadius: 50,
  },
  whiteFont: {
    textTransform: 'uppercase',
    color: '#FFF',
  },
});

const mapDispatchToProps = dispatch => ({
  setExperiencia: exp =>
    dispatch({type: Actions.setProfissionalExperiencia, payload: exp}),
});

export default connect(
  null,
  mapDispatchToProps,
)(Experiencia);
