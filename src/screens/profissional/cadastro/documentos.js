import React, {useState} from 'react';
import {
  ScrollView,
  Alert,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Colors from '../../../assets/colors';
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {IMAGE_PICKER_BASE_CONFIG, URL} from '../../../assets/consts';
import {connect} from 'react-redux';
import {
  Stitch,
  UserPasswordAuthProviderClient,
} from 'mongodb-stitch-react-native-core';
import {sendDocumentToAmazon} from '../../../DAOs/profissional';

const DocumentosCadastroProfissional = props => {
  const [documents, setDocuments] = useState({
    antecedentesCriminales: false,
    seguroCivil: false,
    curriculo: false,
    cbu: false,
    afip: false,
    dni: false,
  });

  const [loading, setLoading] = useState(false);

  const validateForm = form => {
    if (
      form.antecedentesCriminales ||
      form.seguroCivil ||
      form.curriculo ||
      form.cbu ||
      form.afip ||
      form.dni
    ) {
      return true;
    }
    return false;
  };

  const send2server = () => {
    if (validateForm(documents)) {
      setLoading(true);
      let data = {...props.generalData};
      const docsIndexes = [
        'cbu_uri',
        'curriculo_uri',
        'antecedentesCriminales_uri',
        'seguroCivil_uri',
        'afip_uri',
        'dni_uri',
      ];
      data = {
        ...data,
        docs: {
          cbu_uri: documents.cbu.uri,
          curriculo_uri: documents.curriculo.uri,
          antecedentesCriminales_uri: documents.antecedentesCriminales.uri,
          seguroCivil_uri: documents.seguroCivil.uri,
          afip_uri: documents.afip.uri,
          dni_uri: documents.dni.uri,
        },
      };

      const promiseCBU = sendDocumentToAmazon(
        props.generalData.email,
        'cbu',
        data.docs.cbu_uri,
      );

      const promiseCurriculo = sendDocumentToAmazon(
        props.generalData.email,
        'curriculo',
        data.docs.curriculo_uri,
      );

      const promiseAntecedentesCriminales = sendDocumentToAmazon(
        props.generalData.email,
        'antecedentesCriminales',
        data.docs.antecedentesCriminales_uri,
      );
      const promiseSeguroCivil = sendDocumentToAmazon(
        props.generalData.email,
        'seguroCivil',
        data.docs.seguroCivil_uri,
      );
      const promiseAFIP = sendDocumentToAmazon(
        props.generalData.email,
        'afip',
        data.docs.afip_uri,
      );
      const promiseDNI = sendDocumentToAmazon(
        props.generalData.email,
        'dni',
        data.docs.dni_uri,
      );

      const promisesArray = [
        promiseCBU,
        promiseCurriculo,
        promiseAntecedentesCriminales,
        promiseSeguroCivil,
        promiseAFIP,
        promiseDNI,
      ];

      Promise.all(promisesArray)
        .then(resArray => {
          delete data.docs;
          let aux = {};
          resArray.forEach((res, index) => {
            aux[docsIndexes[index]] = res;
          });
          data = {...data, docs_urls: aux};
          if (validateForm(documents)) {
            let data = {...props.generalData};
            delete data.imageUri;
            sendDataToServer(
              {
                ...data,
                preferencias: props.preferencias,
                aceito: false,
                experiencia: props.experiencia,
                tipoProfissional: props.tiposProfissional,
                dateCadastro: new Date(),
                usuario: {
                  email: props.generalData.email.toLowerCase(),
                  password: props.generalData.contrasena,
                  type: 'acompanhante',
                },
              },
              () => {
                console.log('success');
              },
              () => {
                props.navigation.navigate('Home');
                setLoading(false);
              },
              () => {
                setLoading(true);
              },
            );
          } else {
            Alert.alert('Erro', 'Suba todos os documentos');
          }
        })
        .catch(err => {
          alert(JSON.stringify(err));
        });
    }
  };

  const sendDataToServer = (
    data,
    callbackSuccess = () => {},
    callbackLoading = () => {},
  ) => {
    callbackLoading();
    axios
      .post(URL + 'acompanhante/incoming_webhook/createAcompanhante', data)
      .then(result => {
        console.log(result);
        const emailPasswordClient = Stitch.defaultAppClient.auth.getProviderClient(
          UserPasswordAuthProviderClient.factory,
        );
        emailPasswordClient
          .registerWithEmail(data.usuario.email, data.usuario.password)
          .then(() => {
            Alert.alert('Bienvenido a RIAT!', 'Su registro ha sido completado');
            callbackSuccess();
          })
          .catch(err => alert('Error registering new user: ' + err));
      })
      .catch(err => {
        Alert.alert('Erro!', 'Registro nao gravado no banco' + err);
      });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <TouchableOpacity
        style={styles.itemContainter}
        onPress={async () => {
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setDocuments({
                  ...documents,
                  antecedentesCriminales: response,
                });
              }
            },
          );
        }}>
        <Text style={styles.itemText}>Antecedentes Criminales</Text>
        {documents.antecedentesCriminales ? (
          <Icon name="check" color={Colors.colorGreen} size={20} />
        ) : (
          <Icon name="camera" color={Colors.colorBlue} size={20} />
        )}
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.itemContainter}
        onPress={async () =>
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setDocuments({...documents, cbu: response});
              }
            },
          )
        }>
        <Text style={styles.itemText}>CBU</Text>
        {documents.cbu ? (
          <Icon name="check" color={Colors.colorGreen} size={20} />
        ) : (
          <Icon name="camera" color={Colors.colorBlue} size={20} />
        )}
      </TouchableOpacity>
      {/* TODO: certificados AQUI.*/}
      <TouchableOpacity
        style={styles.itemContainter}
        onPress={async () =>
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setDocuments({...documents, afip: response});
              }
            },
          )
        }>
        <Text style={styles.itemText}>Constancia de Inscripción en AFIP</Text>
        {documents.afip ? (
          <Icon name="check" color={Colors.colorGreen} size={20} />
        ) : (
          <Icon name="camera" color={Colors.colorBlue} size={20} />
        )}
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.itemContainter}
        onPress={async () =>
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setDocuments({...documents, curriculo: response});
              }
            },
          )
        }>
        <Text style={styles.itemText}>Curriculum Profesional</Text>
        {documents.curriculo ? (
          <Icon name="check" color={Colors.colorGreen} size={20} />
        ) : (
          <Icon name="camera" color={Colors.colorBlue} size={20} />
        )}
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.itemContainter}
        onPress={async () =>
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setDocuments({...documents, dni: response});
              }
            },
          )
        }>
        <Text style={styles.itemText}>DNI</Text>
        {documents.dni ? (
          <Icon name="check" color={Colors.colorGreen} size={20} />
        ) : (
          <Icon name="camera" color={Colors.colorBlue} size={20} />
        )}
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.itemContainter}
        onPress={async () =>
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setDocuments({...documents, seguroCivil: response});
              }
            },
          )
        }>
        <Text style={styles.itemText}>
          Póliza de Seguro de Responsabilidad Civil
        </Text>
        {documents.seguroCivil ? (
          <Icon name="check" color={Colors.colorGreen} size={20} />
        ) : (
          <Icon name="camera" color={Colors.colorBlue} size={20} />
        )}
      </TouchableOpacity>
      <TouchableOpacity
        disabled={loading}
        onPress={send2server}
        style={[styles.btnNext, loading ? styles.loading : {}]}>
        <Text style={styles.whiteFont}>
          {loading ? 'Carregando...' : 'Enviar'}
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
    marginTop: 25,
  },
  itemContainter: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 25,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  loading: {
    backgroundColor: '#CCC',
  },
  itemText: {
    fontSize: 16,
    marginRight: 5,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 50,
    marginTop: 5,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(98,100,207,1)',
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginVertical: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginBottom: 20,
    backgroundColor: 'rgba(98,100,207,1)',
    borderRadius: 50,
  },
  whiteFont: {
    textTransform: 'uppercase',
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  generalData: state.register.generalData,
  preferencias: state.register.profissionalPreferencias,
  experiencia: state.register.profissionalExperiencia,
  tiposProfissional: state.register.tiposProfissional,
});

export default connect(mapStateToProps)(DocumentosCadastroProfissional);
