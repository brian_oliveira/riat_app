import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';
import {connect} from 'react-redux';
import Actions from '../../../../redux/actions/actions';

const Acompanamientos = props => {
  return (
    <ScrollView style={styles.container}>
      <View style={{flexDirection: 'row', marginTop: 25, marginBottom: 20}}>
        <Icon
          name={'search'}
          style={{marginTop: 15, marginRight: 10}}
          size={18}
        />
        <TextInput style={styles.textInput} placeholder={'Buscar'} />
      </View>
      <Text style={[styles.centerAlign]}>Solicitaciones</Text>
      {props.userLogged.requests && props.userLogged.requests.length > 0 ? (
        props.userLogged.requests.map((request, index) => (
          <TouchableOpacity
            onPress={() => {
              props.selectSolicitacao(request.solicitacao);
              props.navigation.navigate('RecibirSolicitud', {index: index});
            }}
            style={styles.pacienteContainer}>
            <Text style={styles.pacienteName}>
              {request.solicitacao.selectedPaciente.nombreCompleto}
            </Text>
          </TouchableOpacity>
        ))
      ) : (
        <Text>Nenhuma solicitacao encontrada</Text>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F7F7F7',
  },
  centerAlign: {
    textAlign: 'center',
    color: Colors.colorRed,
    marginTop: 15,
    marginBottom: 8,
    fontWeight: 'bold',
    fontSize: 18,
  },
  textInput: {
    backgroundColor: '#FFF',
    borderRadius: 7,
    paddingLeft: 15,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    flex: 1,
    elevation: 4,
  },
  pacienteContainer: {
    backgroundColor: Colors.colorBlue,
    padding: 18,
    borderRadius: 50,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 10,
  },
  pacienteName: {
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  selectSolicitacao: solicitacao =>
    dispatch({type: Actions.selectSolicitacao, payload: solicitacao}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Acompanamientos);
