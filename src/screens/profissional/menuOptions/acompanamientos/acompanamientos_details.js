import React from 'react';
import {View, Text, TextInput} from 'react-navigation';
import moment from 'moment';
import {connect} from 'react-redux';

const CardAcompanamientos = props => {
  return (
    <View>
      <View>
        <Text>{props.title}</Text>
      </View>
      <Text>{props.text}</Text>
    </View>
  );
};

const AcompanamientosDetails = props => {
  const cards = [
    {
      title: 'Datos de la solicitud',
      text:
        'Fecha de inicio ' +
        moment(new Date(props.selectedSolicitacao.fechaInicio)).format(
          'DD/MM/YYYY',
        ) +
        ' \n Fecha de termino: ' +
        moment(
          new Date(props.selectedSolicitacao.fechaFin).format('DD/MM/YYYY'),
        ),
    },
    {
      title: 'Resumen de los datos clínicos',
      text: 'Diagnostico: ' + props.selectedSolicitacao.diagnostico + ' \n',
    },
    {
      title: 'Recomendaciones',
      text:
        'Fecha de inicio xx/xx/xxxx \n Fecha de termino: xx/xx/xxxx \n Costos $$$',
    },
  ];

  return (
    <View>
      <Text>Detales</Text>
      <Text>Pablo Vasquez</Text>
      <Text>
        37 años{'\n'}Status: Activo{'\n'}Buenos Aires, Argentina
      </Text>
      {cards.map(card => (
        <CardAcompanamientos title={card.title} text={card.text} />
      ))}
    </View>
  );
};

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  selectedSolicitacao: state.profissional.selectedSolicitacao,
});

export default connect(mapStateToProps)(AcompanamientosDetails);
