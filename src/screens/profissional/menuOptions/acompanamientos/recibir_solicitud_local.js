import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import Colors from '../../../../assets/colors';
import {connect} from 'react-redux';
import {
  acceptOrRejectProposta,
  prepareRequestsToSendToServer,
} from '../../../../DAOs/profissional';
import Usuarios from '../../../../DAOs/usuarios';
import Actions from '../../../../redux/actions/actions';
import MapView, {Marker} from 'react-native-maps';

const RecibirSolicitud = props => {
  const [local, setLocal] = React.useState({
    latitude: -34.5875885,
    longitude: -58.4027622,
  });

  React.useEffect(() => {
    setLocal({
      latitude: +props.selectedSolicitacao.selectedPaciente.coordinadasDireccion
        .coordinates[0].$numberDouble,
      longitude: +props.selectedSolicitacao.selectedPaciente
        .coordinadasDireccion.coordinates[0].$numberDouble,
    });
  }, [props]);

  return (
    <View style={{flex: 1}}>
      <Text style={styles.name}>
        {props.selectedSolicitacao.selectedPaciente.nombreCompleto}
      </Text>
      <Text style={styles.centerAlign}>
        {props.selectedSolicitacao.selectedPaciente.direccion}
      </Text>
      <Text style={[styles.centerAlign, {marginBottom: 15}]}>
        {props.selectedSolicitacao.horarioInicio +
          ' ~ ' +
          props.selectedSolicitacao.horarioFim}
      </Text>
      {
        // Google maps aqui
      }
      <MapView
        showsUserLocation={true}
        style={styles.map}
        region={{...local, latitudeDelta: 0.09, longitudeDelta: 0.0921}}
        initialRegion={{
          latitude: -34.5875885,
          longitude: -58.4027622,
          latitudeDelta: 0.09,
          longitudeDelta: 0.0921,
        }}>
        <Marker coordinate={local} />
      </MapView>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          onPress={async () => {
            const index = await Number(props.navigation.getParam('index'));
            props.navigation.navigate('RecibirSolicitudDiagnostico', {
              index: index,
            });
          }}
          style={[styles.button, {backgroundColor: Colors.colorGreen}]}>
          <Text style={[styles.centerAlign, {color: '#FFF'}]}>Aceito</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            const index = props.navigation.getParam('index');

            acceptOrRejectProposta(
              {
                accepted: false,
                acompanhanteId: props.userLogged._id,
                newRequests: prepareRequestsToSendToServer(
                  props.userLogged.requests,
                  index,
                ),
              },
              () => {
                Usuarios.updateFromServer(
                  props.userLogged.usuario.uId,
                  props.userLogged.usuario.email,
                  res => {
                    props.logged(res.data);
                  },
                );
              },
            );

            props.navigation.navigate('ProfissionalMenu');
          }}
          style={[styles.button, {backgroundColor: Colors.colorRed}]}>
          <Text style={[styles.centerAlign, {color: '#FFF'}]}>No, gracias</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  buttonsContainer: {
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
  },
  name: {
    marginTop: 15,
    textAlign: 'center',
    fontSize: 18,
    textTransform: 'uppercase',
  },
  centerAlign: {
    textAlign: 'center',
  },
  button: {
    margin: 8,
    flex: 1,
    padding: 15,
    borderRadius: 7,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 4,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  selectedSolicitacao: state.profissional.selectedSolicitacao,
});

const mapDispatchToProps = dispatch => ({
  logged: userLogged => dispatch({type: Actions.logged, payload: userLogged}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RecibirSolicitud);
