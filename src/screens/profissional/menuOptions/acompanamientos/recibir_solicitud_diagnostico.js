import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import Colors from '../../../../assets/colors';

import {Card} from '../../../instituicao/menuOptions/acompanamientos/acompanamientos_detalhe';
import {
  acceptOrRejectProposta,
  prepareRequestsToSendToServer,
} from '../../../../DAOs/profissional';
import Usuarios from '../../../../DAOs/usuarios';
import Actions from "../../../../redux/actions/actions";

const RecibirSolicitudDiagnostico = props => {
  const getFullString = array => {
    let string = '';
    array.forEach(item => {
      string += ' ' + item;
    });
    return string;
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.containerProfile}>
        <Image
          style={styles.profileImage}
          source={{
            uri: `https://riat.s3.amazonaws.com/public/${
              props.selectedSolicitacao.selectedPaciente._id.$oid
            }_paciente_userprofile_image`,
            //uri: 'https://images.unsplash.com/photo-1548365108-908055adb1c8?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
          }}
        />
        <Text style={styles.name}>
          {props.selectedSolicitacao.selectedPaciente.nombreCompleto}
        </Text>
      </View>

      <Card
        title="Datos de la solicitud"
        content={
          'Direccion: ' +
          props.selectedSolicitacao.selectedPaciente.direccion +
          '\n' +
          'Horario: ' +
          props.selectedSolicitacao.horarioInicio +
          ' até ' +
          props.selectedSolicitacao.horarioFim +
          '\n'
        }
      />

      <Card
        title="Area Diagnosticada"
        content={props.selectedSolicitacao.selectedPaciente.diagnostico}
      />

      <Card
        title="Area a Promover"
        content={
          'areasPromover' in props.selectedSolicitacao
            ? getFullString(props.selectedSolicitacao.areasPromover)
            : 'No hay areas a promover'
        }
      />

      <Card
        title="Plan de Actividades"
        content={
          'planActividades' in props.selectedSolicitacao
            ? props.selectedSolicitacao.planActividades
            : 'No hay un plan de Actividades'
        }
      />

      <Card
        title="Observaciones"
        content={
          'observaciones' in props.selectedSolicitacao
            ? props.selectedSolicitacao.observaciones
            : 'No hay observaciones'
        }
      />

      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          onPress={async () => {
            const index = await Number(props.navigation.getParam('index'));

            acceptOrRejectProposta(
              {
                accepted: true,
                acompanhanteId: props.userLogged._id,
                newRequests: prepareRequestsToSendToServer(
                  props.userLogged.requests,
                  index,
                ),
                solicitacaoAceita: {
                  ...props.userLogged.requests[index],
                  solicitacao: props.userLogged.requests[index].solicitacao._id,
                },
              },
              () => {
                Usuarios.updateFromServer(
                  props.userLogged.usuario.uId,
                  props.userLogged.usuario.email,
                  res => {
                    props.logged(res.data);
                  },
                );
              },
            );
            props.navigation.navigate('ProfissionalMenu');
          }}
          style={[styles.button, {backgroundColor: Colors.colorGreen}]}>
          <Text style={[styles.centerAlign, {color: '#FFF'}]}>Aceito</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            const index = props.navigation.getParam('index');

            acceptOrRejectProposta(
              {
                accepted: false,
                acompanhanteId: props.userLogged._id,
                newRequests: prepareRequestsToSendToServer(
                  props.userLogged.requests,
                  index,
                ),
              },
              () => {
                Usuarios.updateFromServer(
                  props.userLogged.usuario.uId,
                  props.userLogged.usuario.email,
                  res => {
                    props.logged(res.data);
                  },
                );
              },
            );
            props.navigation.navigate('ProfissionalMenu');
          }}
          style={[styles.button, {backgroundColor: Colors.colorRed}]}>
          <Text style={[styles.centerAlign, {color: '#FFF'}]}>No, gracias</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20,
  },
  containerProfile: {
    alignItems: 'center',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 10,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  centerAlign: {
    textAlign: 'center',
  },
  cardContainer: {
    flex: 1,
    width: '100%',
    borderColor: '#eee',
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 10,
  },
  cardHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorBlue,
    borderRadius: 4,
    flex: 3,
    height: 48,
  },
  cardBodyContainer: {
    flex: 1,
    padding: 5,
  },
  cardTextHeader: {
    fontSize: 16,
    color: '#FFF',
  },
  buttonText: {
    fontSize: 14,
    color: '#FFF',
    marginLeft: 5,
  },
  buttonsContainer: {
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
  },
  button: {
    margin: 2,
    flex: 1,
    padding: 15,
    borderRadius: 7,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 4,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  selectedSolicitacao: state.profissional.selectedSolicitacao,
});

const mapDispatchToProps = dispatch => ({
  logged: userLogged => dispatch({type: Actions.logged, payload: userLogged}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RecibirSolicitudDiagnostico);
