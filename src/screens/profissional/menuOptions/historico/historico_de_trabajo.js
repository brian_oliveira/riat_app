import React, {useState, useEffect} from 'react';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import {View, ScrollView, Text, StyleSheet} from 'react-native';
import Actions from '../../../../redux/actions/actions';
import {connect} from 'react-redux';
import {getSessoesFromAcompanhante} from '../../../../DAOs/profissional';
import moment from 'moment';

const TrabajoComponent = props => {
  // TODO: Bolinha abaixo do dia;
  // TODO: Picker para selecionar o Paciente

  return (
    <View style={[styles.cardTrabajo]}>
      <Text style={styles.pacienteName}>
        {props.sessao.paciente.nombreCompleto}
      </Text>
      <Text>Calle Laprida, 3563</Text>
      <View style={{marginTop: 10}}>
        <Text style={styles.secondaryText}>
          Iniciado a las{' '}
          {moment(new Date(props.sessao.horaInicio)).format('HH:mm')}
        </Text>
        <Text style={styles.secondaryText}>
          Finalizado a las{' '}
          {moment(new Date(props.sessao.horarioFim)).format('HH:mm')}
        </Text>
        <Text style={styles.secondaryText}>
          Horas contadas:{' '}
          {moment.utc(moment(new Date(props.sessao.horarioFim))
            .diff(moment(new Date(props.sessao.horaInicio)))).format('HH:mm')}
        </Text>
      </View>
    </View>
  );
};

const HistoricoDeTrabajo = props => {
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    LocaleConfig.locales.es = {
      monthNames: [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
      ],
      monthNamesShort: [
        'Ene',
        'Feb',
        'Mar',
        'Abr',
        'May',
        'Jun',
        'Jul',
        'Ago',
        'Sep',
        'Oct',
        'Nov',
        'Dic',
      ],
      dayNames: [
        'Domingo',
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sábado',
      ],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      today: 'Hoy',
    };
    LocaleConfig.defaultLocale = 'es';
  });

  useEffect(() => {
    getSessoesFromAcompanhante(props.userLogged._id, res => {
      props.setSessoesFromAcompanhante(res.data);
    });
  }, [props, props.userLogged]);

  return (
    <ScrollView style={styles.container}>
      <Calendar
        style={{marginTop: 15}}
        current={date}
        onDayPress={day => {
          setDate(day.dateString);
        }}
        markedDates={{[date]: {selected: true}}}
        monthFormat={'MMM / yyyy'}
      />
      <Text style={styles.sesionesDelDia}>Sesiones del Día</Text>
      <Text style={[styles.sesionesDelDia, {fontSize: 14, marginBottom: 15}]}>
        {moment(date).format('DD/MMM/YYYY')}
      </Text>
      {props.sessoes.map(sessao => {
        if (moment(new Date(sessao.horaInicio)).isSame(moment(date), 'day')) {
          return <TrabajoComponent sessao={sessao} />;
        } else {
          return <></>;
        }
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5,
    flex: 1,
    backgroundColor: '#FFF',
  },
  sesionesDelDia: {
    fontSize: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  pacienteName: {
    fontSize: 18,
    textTransform: 'uppercase',
  },
  secondaryText: {
    fontSize: 13,
    color: '#CCC',
  },
  cardTrabajo: {
    padding: 10,
    backgroundColor: '#FFF',
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 0,
    borderColor: '#000',
    borderRadius: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 15,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  sessoes: state.profissional.sessoes,
});

const mapDispatchToProps = dispatch => ({
  setSessoesFromAcompanhante: sessoes =>
    dispatch({type: Actions.setSessoesFromAcompanhante, payload: sessoes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HistoricoDeTrabajo);
