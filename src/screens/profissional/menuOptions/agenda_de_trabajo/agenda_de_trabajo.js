/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../../../assets/colors';
import {connect} from 'react-redux';
import {MESES} from '../../../../assets/consts';
import Geolocation from '@react-native-community/geolocation';
import {checkInToServer, checkOutToServer} from '../../../../DAOs/profissional';
import {checkIfDateIsToday} from '../../../../assets/utils';
import moment from 'moment/min/moment-with-locales';
import axios from 'axios';
import {
  performLoginOnServer,
  updateFromServer,
} from '../../../login/performLogin';
import {UserPasswordCredential} from 'mongodb-stitch-core-sdk';
import Actions from '../../../../redux/actions/actions';
import Usuarios from '../../../../DAOs/usuarios';
const dayWeeks = ['DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB'];

const getFirstDayOfWeek = () => {
  var curr = new Date();
  var first = curr.getDate() - curr.getDay();
  return new Date(curr.setDate(first));
};

const renderDay = (date, addDays) => {
  let newDate = date;
  newDate.setDate(date.getDate() + addDays);
  return newDate;
};

const TrabajoComponent = props => {
  const checkIfSessionIsRunning = () => {
    let x = false;

    props.userLogged.sessoes.forEach(sessao => {
      if (
        sessao.solicitacaoId.$oid === props.aceitas.solicitacao._id.$oid &&
        checkIfDateIsToday(new Date(sessao.horaInicio))
      ) {
        x = true;
      }
    });

    return x;
  };

  const checkIfAlreadyFinished = () => {
    let x = false;

    props.userLogged.sessoes.forEach(sessao => {
      if (
        sessao.solicitacaoId.$oid === props.aceitas.solicitacao._id.$oid &&
        new Date(sessao.horaInicio).getDay() === new Date().getDay()
      ) {
        x = sessao.finalizado;
      }
    });
    return x;
  };

  const [running, setRunning] = React.useState(checkIfSessionIsRunning());
  const [finished, setFinished] = React.useState(checkIfAlreadyFinished());

  const doCheckIn = () => {
    Geolocation.getCurrentPosition(info => {
      checkInToServer({
        solicitacaoId: props.aceitas.solicitacao._id,
        acompanhante: props.userLogged._id,
        horaInicio: new Date(),
        finalizado: false,
        coordinadasInicio: {
          type: 'Point',
          coordinates: [info.coords.latitude, info.coords.longitude],
        },
      });
      Usuarios.updateFromServer(
        props.userLogged.usuario.uId,
        props.userLogged.usuario.email,
        res => {
          props.logged(res.data);
        },
      );
      setRunning(true);
    });
  };

  const getSessionId = () => {
    let id = null;
    props.userLogged.sessoes.forEach(sessao => {
      if (sessao.solicitacaoId.$oid === props.aceitas.solicitacao._id.$oid) {
        id = sessao._id;
      }
    });
    return id;
  };

  const goToInformes = () => {
    props.navigation.navigate('InformesMenu');
  };

  const doCheckOut = async () => {
    const {data} = await axios.post(
      'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/riat-sfhra/service/paciente/incoming_webhook/getValorHoraFromPaciente',
    );
    checkOutToServer(
      {
        sessaoId: getSessionId(),
        changes: {
          finalizado: true,
          horarioFim: new Date(),
          informeDone: false,
          valorHoraAT: data.valorHora,
        },
      },
      res => {
        Usuarios.updateFromServer(
          props.userLogged.usuario.uId,
          props.userLogged.usuario.email,
          res => {
            props.logged(res.data);
          },
        );
      },
    );
  };

  React.useEffect(() => {
    setRunning(checkIfSessionIsRunning());
    setFinished(checkIfAlreadyFinished());
  }, [props.userLogged]);

  return (
    <View style={styles.cardTrabajo}>
      <Text style={[styles.text, {fontSize: 17, textTransform: 'uppercase'}]}>
        {props.aceitas.solicitacao.selectedPaciente.nombreCompleto}
      </Text>
      <Text style={styles.text}>
        {props.aceitas.solicitacao.selectedPaciente.direccion}
        {'\n'}
        {props.aceitas.horarioInicio} ~ {props.aceitas.horarioFim}
      </Text>
      <View style={styles.containerButtons}>
        {running ? (
          finished ? (
            <></>
          ) : (
            <TouchableOpacity style={styles.button}>
              <Text style={[styles.buttonText, {color: '#999'}]}>Cancelar</Text>
            </TouchableOpacity>
          )
        ) : (
          <></>
        )}
        {props.isToday ? (
          <TouchableOpacity
            onPress={
              running ? (finished ? goToInformes : doCheckOut) : doCheckIn
            }
            style={[
              styles.button,
              styles.shadow,
              running
                ? finished
                  ? {backgroundColor: Colors.colorBlue}
                  : {backgroundColor: Colors.colorRed}
                : {backgroundColor: Colors.colorGreen},
            ]}>
            <Text style={[styles.buttonText, {color: '#FFF'}]}>
              {running ? (finished ? 'Informe' : 'Finalizar') : 'Iniciar'}
            </Text>
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

const AgendaDeTrabajo = props => {
  return (
    <ScrollView style={styles.container}>
      {dayWeeks.map((day, index) => (
        <View style={[styles.separatorRow, {marginTop: index === 0 ? 25 : 0}]}>
          <View style={{paddingRight: 5}}>
            <Text style={styles.textWeekday}>{day}</Text>
            <Text style={styles.textWeekday}>
              {moment(getFirstDayOfWeek())
                .add(index, 'day')
                .format('DD/\nMM')}
            </Text>
          </View>
          <View style={{flex: 1}}>
            {props.userLogged.solicitacoesAceitas.map(aceitas => {
              if ('dias' in aceitas) {
                return aceitas.dias[index] ? (
                  <TrabajoComponent
                    isToday={index === new Date().getDay()}
                    aceitas={aceitas}
                    stitchClient={props.stitchClient}
                    navigation={props.navigation}
                    userLogged={props.userLogged}
                    logged={props.logged}
                  />
                ) : (
                  <></>
                );
              } else {
                return (
                  <Text style={{color: '#999', marginBottom: 50}}>
                    Todavía no hay solicitaciones para este día.
                  </Text>
                );
              }
            })}
          </View>
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    backgroundColor: '#FCFCFC',
    paddingRight: 15,
  },
  separatorRow: {
    flex: 1,
    flexDirection: 'row',
  },
  textWeekday: {
    fontWeight: 'bold',
    color: '#999',
    textAlign: 'right',
  },
  text: {
    color: '#707070',
  },
  shadow: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    elevation: 3,
    shadowRadius: 4.65,
  },
  cardTrabajo: {
    padding: 10,
    backgroundColor: '#FFF',
    marginLeft: 5,
    marginRight: 5,
    flex: 1,
    borderWidth: 0,
    borderColor: '#000',
    borderRadius: 7,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 15,
  },
  containerButtons: {
    flex: 1,
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    margin: 10,
    backgroundColor: '#FFF',
    padding: 7,
    borderRadius: 5,
  },
  buttonText: {
    textTransform: 'uppercase',
    textAlign: 'center',
  },
});

const mapStateToProps = state => ({
  stitchClient: state.general.stitchClient,
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  logged: user => dispatch({type: Actions.logged, payload: user}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AgendaDeTrabajo);
