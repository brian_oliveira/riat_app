/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../assets/colors';
import {MESES_EXTENSO} from '../../../../assets/consts';
import {pad} from '../../../../assets/utils';
import {sendInformeToServer} from '../../../../DAOs/profissional';
import Usuarios from '../../../../DAOs/usuarios';
import Actions from '../../../../redux/actions/actions';
import {connect} from 'react-redux';

const IncrementDecrement = props => {
  return (
    <View style={styles.row}>
      <TouchableOpacity
        onPress={() => {
          if (props.informeData[props.type][props.index].nota < 4) {
            let aux = {...props.informeData};
            let array = aux[props.type]; //selecionando apenas registros de um certo tipo
            array[props.index].nota = array[props.index].nota + 1;
            props.changeNota(aux);
          }
        }}
        style={{marginBottom: 3, marginTop: 3}}>
        <Icon name={'minus'} size={12} style={{padding: 5}} />
      </TouchableOpacity>
      <Text style={{fontSize: 18, marginLeft: 10, marginRight: 10}}>
        {props.informeData[props.type][props.index].nota}
      </Text>
      <TouchableOpacity
        onPress={() => {
          if (props.informeData[props.type][props.index].nota > 0) {
            let aux = {...props.informeData};
            let array = aux[props.type];
            array[props.index].nota = array[props.index].nota - 1;
            props.changeNota(aux);
          }
        }}
        style={{marginBottom: 3, marginTop: 3}}>
        <Icon name={'plus'} size={12} style={{padding: 5}} />
      </TouchableOpacity>
    </View>
  );
};

const registroAcompanante = [
  'Nivel de intervención necesaria',
  'Nivel de conexión con el contexto del paciente',
  'Como puntuarías tu estado de ánimo hoy?',
  'Qué puntaje atribuyes a tu comomidad con el paciente hoy?',
];

const Actividad = props => {
  return (
    <View style={styles.row}>
      <Text style={{flex: 1}}>
        {
          props.informeData[props.type][props.index][
            props.type === 'areasPromover' ? 'area' : 'registro'
          ]
        }
      </Text>
      <IncrementDecrement
        index={props.index}
        type={props.type}
        informeData={props.informeData}
        changeNota={props.changeNota}
      />
    </View>
  );
};

const Informe = props => {
  // TODO: Fazer modal de agradecimento

  const [solicitacao, setSolicitacao] = React.useState(
    props.navigation.getParam('solicitacao'),
  );
  const [sessao, setSessao] = React.useState(
    props.navigation.getParam('sessao'),
  );

  const prepareData = () => {
    let areasPromoverAux = [];
    let registroAcompanhanteAux = [];

    registroAcompanante.forEach(registro =>
      registroAcompanhanteAux.push({registro: registro, nota: 0}),
    );
    if (solicitacao.areasPromover && solicitacao.areasPromover.length > 0) {
      solicitacao.areasPromover.forEach(area =>
        areasPromoverAux.push({area: area, nota: 0}),
      );

      return {
        registroAcompanante: registroAcompanhanteAux,
        areasPromover: areasPromoverAux,
        observaciones: '',
      };
    }
    return {
      registroAcompanante: registroAcompanhanteAux,
      areasPromover: null,
      observaciones: '',
    };
  };

  const [informeData, setInformeData] = useState(prepareData());

  const getHourStringFromDate = date => {
    return pad(date.getHours(), 2) + ':' + pad(date.getMinutes(), 2);
  };

  return (
    <ScrollView style={styles.container}>
      <Text
        style={{
          marginTop: 20,
          fontSize: 20,
          textAlign: 'center',
          textTransform: 'uppercase',
        }}>
        {solicitacao.selectedPaciente.nombreCompleto}
      </Text>
      <Text style={{textAlign: 'center'}}>
        {new Date(sessao.horaInicio).getDate()} de{' '}
        {MESES_EXTENSO[new Date(sessao.horaInicio).getMonth()]}
      </Text>
      <Text style={{textAlign: 'center'}}>
        {getHourStringFromDate(new Date(sessao.horaInicio))} ~{' '}
        {getHourStringFromDate(new Date(sessao.horarioFim))}
      </Text>
      <Text style={[styles.centerText, styles.title]}>
        Registro del Acompañante
      </Text>
      <View>
        {informeData.registroAcompanante.map((pregunta, index) => (
          <Actividad
            informeData={informeData}
            index={index}
            type={'registroAcompanante'}
            changeNota={setInformeData}
            key={index}
          />
        ))}
      </View>
      <Text style={[styles.centerText, styles.title]}>Areas a Promover</Text>
      <View>
        {solicitacao.areasPromover ? (
          solicitacao.areasPromover.map((area, index) => (
            <Actividad
              informeData={informeData}
              index={index}
              type={'areasPromover'}
              changeNota={setInformeData}
            />
          ))
        ) : (
          <Text>Areas a promover no disponibles en el momento</Text>
        )}
      </View>
      <Text style={[styles.centerText, styles.title]}>Escriba su Informe</Text>
      <TextInput
        style={styles.observaciones}
        onChangeText={text =>
          setInformeData({...informeData, observaciones: text})
        }
        placeholder={'Informe aquí las observaciones de la sesión.'}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          sendInformeToServer(
            {
              sessaoId: sessao._id,
              relatorio: informeData,
            },
            () => {
              props.navigation.navigate('InformesMenu');
              Usuarios.updateFromServer(
                props.userLogged.usuario.uId,
                props.userLogged.usuario.email,
                res => {
                  props.logged(res.data);
                },
              );
            },
          );
        }}>
        <Text style={{color: '#FFF', textAlign: 'center'}}>Enviar</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    flex: 1,
    backgroundColor: '#FCFCFC',
  },
  row: {
    flexDirection: 'row',
  },
  centerText: {
    textAlign: 'center',
  },
  title: {
    fontSize: 18,
    marginBottom: 7,
    marginTop: 18,
  },
  button: {
    flex: 1,
    margin: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    backgroundColor: Colors.colorBlue,
    padding: 7,
    borderRadius: 5,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  selectedSolicitacao: state.profissional.selectedSolicitacao,
});

const mapDispatchToProps = dispatch => ({
  logged: userLogged => dispatch({type: Actions.logged, payload: userLogged}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Informe);
