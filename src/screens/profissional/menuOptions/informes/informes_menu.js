/* eslint-disable prettier/prettier,react-hooks/exhaustive-deps */
import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Colors from '../../../../assets/colors';
import {connect} from 'react-redux';
import {MESES} from '../../../../assets/consts';
import {getHourMinutesStringDifference, pad} from '../../../../assets/utils';

const CardInforme = props => {

  const getSolicitacaoFromSessao = () => {
    let x = '';
    props.solicitacoesAceitas.forEach(solicitacao => {
      if (
        solicitacao.solicitacao._id.$oid === props.sessao.solicitacaoId.$oid
      ) {
        x = solicitacao.solicitacao;
      }
    });
    return x;
  };

  const [solicitacao, setSolicitacao] = React.useState(
    getSolicitacaoFromSessao(),
  );

  React.useEffect(() => {
    setSolicitacao(getSolicitacaoFromSessao());
  }, []);

  return (
    <View style={styles.cardTrabajo}>
      <Text
        style={[
          styles.text,
          {fontSize: 17, textTransform: 'uppercase', fontWeight: 'bold'},
        ]}>
        {solicitacao.selectedPaciente.nombreCompleto}
      </Text>
      <Text style={styles.text}>
        {solicitacao.selectedPaciente.direccion}{'\n'}
        {new Date(props.sessao.horaInicio).getDate() + ' / ' + MESES[new Date(props.sessao.horaInicio).getMonth()]} -
        {pad(new Date(props.sessao.horaInicio).getHours(), 2) +
        ':'
        + pad(new Date(props.sessao.horaInicio).getMinutes(), 2)}
         ~ {pad(new Date(props.sessao.horarioFim).getHours(), 2) +
      ':'
      + pad(new Date(props.sessao.horarioFim).getMinutes(), 2)}
    </Text>
      <View>
        <Text style={styles.secondaryText}>Iniciado a las {pad(new Date(props.sessao.horaInicio).getHours(), 2) + ':' + pad(new Date(props.sessao.horaInicio).getMinutes(), 2)}</Text>
        <Text style={styles.secondaryText}>Finalizado a las {pad(new Date(props.sessao.horarioFim).getHours(), 2) + ':' + pad(new Date(props.sessao.horarioFim).getMinutes(),2)}</Text>
        <Text style={styles.secondaryText}>Horas contadas: {getHourMinutesStringDifference(new Date(props.sessao.horarioFim), new Date(props.sessao.horaInicio))}</Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Informe', {solicitacao: solicitacao, sessao: props.sessao});
        }}
        style={styles.button}>
        <Text style={styles.buttonText}>Realizar Informe</Text>
      </TouchableOpacity>
    </View>
  );
};

const InformesMenu = props => {

  const [pendentes, setPendentes] = React.useState([]);

  React.useEffect(() => {
    let sessoesPendentes = [];
    props.userLogged.sessoes.forEach((sessao) => {
      if (sessao.finalizado && !sessao.informeDone) {
        sessoesPendentes.push(sessao);
      }
    });
    setPendentes(sessoesPendentes);
  }, [props]);


  return (
    <ScrollView style={styles.container}>
      {pendentes.length > 0 ? (pendentes.map((sessao, index) => {
          return (
              <CardInforme
                  sessao={sessao}
                  navigation={props.navigation}
                  solicitacoesAceitas={props.userLogged.solicitacoesAceitas}
                  key={index} />) }
                  )
          )
            : (<View><Text style={{textAlign: 'center', marginTop: 15}}>Felicitaciones! Usted no tiene ningún informe que hacer.</Text></View>)
        }
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#FCFCFC',
  },
  text: {
    color: '#707070',
  },
  cardTrabajo: {
    padding: 10,
    marginTop: 10,
    backgroundColor: '#FFF',
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 0,
    borderColor: '#000',
    borderRadius: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    marginBottom: 15,
  },
  secondaryText: {
    fontSize: 13,
    color: '#CCC',
  },
  button: {
    flex: 1,
    margin: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 3,
    backgroundColor: Colors.colorBlue,
    padding: 7,
    borderRadius: 5,
  },
  buttonText: {
    textTransform: 'uppercase',
    textAlign: 'center',
    color: '#FFF',
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

export default connect(mapStateToProps)(InformesMenu);
