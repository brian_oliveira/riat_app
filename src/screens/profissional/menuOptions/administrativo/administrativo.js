import React from 'react';
import {
  ScrollView,
  View,
  Text,
  Picker,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../../../../assets/colors';
import {
  calcularHorasTrabalhadas,
  getValorTotal,
} from '../../../../assets/utils';
import {connect} from 'react-redux';
import moment from 'moment/min/moment-with-locales';

const TrabajoComponent = props => {
  const getSolicitacaoFromSessao = () => {
    let x = '';
    props.userLogged.solicitacoesAceitas.forEach(solicitacao => {
      if (
        solicitacao.solicitacao._id.$oid === props.sessao.solicitacaoId.$oid
      ) {
        x = solicitacao.solicitacao;
      }
    });
    return x;
  };

  const [solicitacao, setSolicitacao] = React.useState(
    getSolicitacaoFromSessao(),
  );

  return (
    <View style={styles.containerTrabajo}>
      <Text style={{fontSize: 20, textTransform: 'uppercase'}}>
        {solicitacao.selectedPaciente.nombreCompleto}
      </Text>
      <Text>
        {moment(props.sessao.horaInicio)
          .locale('es')
          .format('dddd, DD/MM/YYYY') + '\n'}
        Horas Trabajadas:{' '}
        {moment
          .utc(
            moment(new Date(props.sessao.horarioFim)).diff(
              moment(new Date(props.sessao.horaInicio)),
            ),
          )
          .format('HH:mm') + '\n'}
        Valor Neto de la hora: ${solicitacao.valorHoraAT}
        {'\n'}
        Monto Total: $
        {solicitacao.valorHoraAT *
          moment.utc(
            moment(new Date(props.sessao.horarioFim)).diff(
              moment(new Date(props.sessao.horaInicio)),
              'hours',
            ),
          )}
      </Text>
      {/*<Text>Estimativa de Pago: 21/05/2019</Text>*/}
    </View>
  );
};

const Administrativo = props => {
  const getAllMonthsWorked = () => {
    let actualMoment = moment(new Date());
    let diffMonths = moment(new Date()).diff(
      moment(new Date(props.userLogged.dateCadastro)),
      'months',
    );
    let array = [];

    for (let i = 0; i <= diffMonths; i++) {
      array.push(actualMoment.add(i, 'months'));
    }

    return array;
  };

  return (
    <ScrollView style={styles.container}>
      <Picker style={{marginTop: 15}}>
        {getAllMonthsWorked().map(month => (
          <Picker.Item label={month.locale('es').format('MMMM/YYYY')} />
        ))}
      </Picker>
      <LinearGradient
        style={styles.header}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[Colors.colorBlue, Colors.colorLightBlue]}>
        <Text style={[styles.headerText, {fontSize: 15}]}>A Recibir:</Text>
        <Text style={[styles.headerText, {fontSize: 45}]}>
          $
          {'sessoes' in props.userLogged
            ? getValorTotal(props.userLogged.sessoes, 150)
            : 0}
        </Text>
        <Text style={[styles.headerText]}>Horas trabajadas: 300h</Text>
      </LinearGradient>
      <Text
        style={{
          textTransform: 'uppercase',
          color: '#AAA',
          marginTop: 15,
          marginBottom: 10,
          marginLeft: 15,
          fontSize: 20,
        }}>
        Detalles
      </Text>
      {props.userLogged.sessoes.map(sessao => (
        <TrabajoComponent userLogged={props.userLogged} sessao={sessao} />
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  header: {
    justifyContent: 'center',
    marginTop: 20,
    shadowColor: '#000',
    marginLeft: 15,
    marginRight: 15,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    padding: 15,
    borderRadius: 10,
  },
  headerText: {
    textTransform: 'uppercase',
    textAlign: 'center',
    color: '#FFFFFF',
  },
  containerTrabajo: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    borderRadius: 5,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    padding: 10,
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 8,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

export default connect(mapStateToProps)(Administrativo);
