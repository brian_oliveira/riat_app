/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ImageBackground,
  Dimensions,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../assets/colors';
import Actions from '../../redux/actions/actions';
import {setPacientesToRedux} from '../../DAOs/profissional';
import {connect} from 'react-redux';
//TODO: Colocar novas solicitacoes como noticias
const options = [
  {
    text: 'Pacientes',
    image: 'users',
    local: 'PacienteListagem',
  },
  {
    text: 'Escribir Informes',
    image: 'file',
    local: 'InformesMenu',
  },
  {
    text: 'Administrativo',
    image: 'dollar-sign',
    local: 'Administrativo',
  },
  {
    text: 'Agenda de Trabajo',
    image: 'calendar-check',
    local: 'AgendaDeTrabajo',
  },
  {
    text: 'Historico de Trabajo',
    image: 'clock',
    local: 'HistoricoDeTrabajo',
  },
  {
    text: 'Chat',
    image: 'comments',
    local: 'ChatRooms',
  },
];

const ProfissionalMenu = props => {
  React.useEffect(() => {
    props.setPacientes(setPacientesToRedux(props));
  }, []);

  return (
    <ImageBackground
      source={require('../../assets/splash_screen.png')}
      style={[{width: '100%', height: '100%'}]}
      imageStyle={{opacity: 0.3}}>
      <View>
        <Image
          style={[styles.logo, {marginTop: 38}]}
          source={require('../../assets/logo_riat.png')}
        />
        <Image
          style={[styles.logo, {height: Dimensions.get('window').height * 0.1}]}
          source={require('../../assets/logo_riat_nome.png')}
        />
        <View style={styles.serviciosContainer}>
          {props.userLogged.requests.length > 0 ? (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('Acompanamientos');
              }}>
              <Icon name={'newspaper'} size={35} color={Colors.colorBlue} />
              <Icon
                  size={18}
                  style={styles.iconExclamation}
                  name={'exclamation-circle'}
                  color={Colors.colorRed}
              />
            </TouchableOpacity>
          ) : (
            <></>
          )}
        </View>
        <View style={styles.row}>
          {options.map((option, index) => (
            <TouchableOpacity
              style={styles.iconOption}
              onPress={() => props.navigation.navigate(option.local)}>
              <Icon name={option.image} size={50} color={Colors.colorBlue} />
              <Text
                style={{
                  fontSize: 15,
                  textAlign: 'center',
                  marginTop: 10,
                  color: Colors.colorRed,
                }}>
                {option.text}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  iconExclamation: {
    position: 'absolute',
    left: -10,
    top: -3,
  },
  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 30,
  },
  logo: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').height * 0.2,
    resizeMode: 'contain',
    margin: 0,
    alignSelf: 'center',
  },
  serviciosContainer: {
    position: 'absolute',
    top: 15,
    right: 15,
    fontSize: 18,
  },
  img: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  iconOption: {
    width: Dimensions.get('window').width * 0.33,
    padding: 10,
    alignItems: 'center',
  },
  icon: {
    fontSize: 30,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
  pacientes: state.paciente.pacientes,
});

const mapDispatchToProps = dispatch => ({
  setPacientes: pacientes =>
    dispatch({type: Actions.setPacientesGeral, payload: pacientes}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfissionalMenu);
