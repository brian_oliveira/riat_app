/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import axios from 'axios';
import {RectButton} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {URL} from '../../assets/consts';
import Colors from '../../assets/colors';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import {getPacientes} from '../../DAOs/instituicao';

const Paciente = props => {
  return (
    <View style={{marginBottom: 15}}>
      <View style={styles.patientContainer}>
        <Image style={styles.patientThumbnail} source={{uri: props.avatar}} />
        <Text style={styles.patientTextName}>
          {props.name}
          <Text style={{fontWeight: 'normal'}}>{`, ${props.age}`}</Text>
        </Text>
        <Text numberOfLines={2} style={styles.patientAddressText}>
          {props.address}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.profileButton}
        onPress={() => props.navigation.navigate('PacienteDetalhe')}>
        <Text style={styles.profileButtonText}>Ver</Text>
      </TouchableOpacity>
    </View>
  );
};

const Pacientes = props => {
  const [search, setSearchString] = useState({
    patientSearch: '',
  });

  return (
    <View style={styles.container}>
      <View style={styles.searchForm}>
        <TextInput
          style={styles.searchInput}
          placeholder={'Buscar paciente...'}
          placeholderTextColor="#999"
          value={search.patientSearch}
          onChangeText={value =>
            setSearchString({...search, patientSearch: value})
          }
        />
        <TouchableOpacity style={styles.searchButton}>
          <Icon name="search" size={20} color="#999" />
        </TouchableOpacity>
      </View>
      <FlatList
        data={
          props.userLogged.userType === 'funcionario'
            ? props.pacientes
            : props.pacientesFam
        }
        keyExtractor={item => `${item._id.$oid}`}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <View style={{marginBottom: 15}}>
            <View style={styles.patientContainer}>
              <Text style={styles.patientTextName}>{item.nombreCompleto}</Text>
            </View>
            <TouchableOpacity
              style={styles.profileButton}
              onPress={() => {
                if (props.userLogged.userType === 'funcionario') {
                  props.selectPacienteInstituicao(item);
                }
                if (props.userLogged.userType === 'familiar') {
                  props.selectPacienteFamiliar(item);
                }
                props.navigation.navigate('PacienteDetalhe');
              }}>
              <Text style={styles.profileButtonText}>Ver</Text>
            </TouchableOpacity>
          </View>
        )}
      />
      <View style={styles.footer}>
        <TouchableOpacity
          style={styles.addPatientButton}
          onPress={() => props.navigation.navigate('CadastroApenasPaciente')}>
          <Icon name="user-plus" size={20} color="#FFF" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

//TODO: FAZER HISTÓRICO DE PACIENTES AQUI DOS PACIENTES.

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginTop: 5,
  },
  footer: {
    position: 'absolute',
    right: 15,
    bottom: 15,
    alignItems: 'flex-end',
  },
  addPatientButton: {
    backgroundColor: Colors.colorRed,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
  },
  searchForm: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingBottom: 20,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  searchInput: {
    flex: 1,
    height: 40,
    backgroundColor: '#eee',
    borderRadius: 4,
    marginLeft: 10,
    paddingRight: 12,
    color: '#999',
  },
  searchButton: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    borderRadius: 4,
    marginLeft: 10,
  },
  patientContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  patientThumbnail: {
    width: 64,
    height: 64,
    borderRadius: 32,
    backgroundColor: '#eee',
  },
  patientTextName: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  patientAddressText: {
    textAlign: 'center',
  },
  profileButton: {
    marginTop: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    borderRadius: 4,
    backgroundColor: Colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
  },
  profileButtonText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFF',
    textTransform: 'uppercase',
  },
});

const mapStateToProps = state => ({
  pacientes: state.instituicao.pacientes,
  pacientesFam: state.familiar.pacientes,
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  selectPacienteFamiliar: paciente =>
    dispatch({type: Actions.selectPacienteFamiliar, payload: paciente}),
  selectPacienteInstituicao: paciente =>
    dispatch({type: Actions.selectPacienteInstituicao, payload: paciente}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Pacientes);
