import React, {useState} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  Alert,
  TouchableOpacity,
  StyleSheet,
  Image,
  View,
  Dimensions,
} from 'react-native';
import Actions from '../../redux/actions/actions';
import {connect} from 'react-redux';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {createPacienteFamiliar} from '../../DAOs/familiar';
import {createPaciente} from '../../DAOs/instituicao';
import {IMAGE_PICKER_BASE_CONFIG} from '../../assets/consts';
import MapView, {Marker} from 'react-native-maps';
import axios from 'axios';
import DatePicker from 'react-native-datepicker';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const CadastroPaciente = props => {
  const [loading, setLoading] = useState(false);
  const [localSelected, selectPlace] = useState({
    destination: {
      latitude: -34.5875885,
      longitude: -58.4027622,
    },
  });

  const [pacienteData, setData] = useState({
    nombreCompleto: '',
    fechaDeNacimiento: '',
    direccion: '',
    DNI: '',
    diagnostico: '',
    psiquiatra: '',
    telefonoPsiquiatra: '',
    psicologo: '',
    telefonoPsicologo: '',
    temPsi: true,
    imgUri: '',
    pacienteUserprofileImage: '',
  });

  const validateForm = form => {
    if (
      form.nombreCompleto.length > 0 &&
      form.DNI.length > 0 &&
      form.diagnostico.length > 0 &&
      'title' in localSelected
    ) {
      console.log('tudo certo');
      return true;
    } else {
      console.log('faltando dado');
      return false;
    }
  };

  const [busca, setBusca] = useState({
    queryString: '',
    result: null,
    error: false,
  });

  const [searchLocation, setSearchLocation] = useState(false);

  return (
    <ScrollView style={styles.container} keyboardShouldPersistTaps={'handled'}>
      <TouchableOpacity
        style={styles.imageContainer}
        onPress={async () => {
          await ImagePicker.showImagePicker(
            IMAGE_PICKER_BASE_CONFIG,
            response => {
              console.log(response);
              if (!response.didCancel) {
                setData({
                  ...pacienteData,
                  imgUri: response.uri,
                  pacienteUserprofileImage: response.data,
                });
              }
            },
          );
        }}>
        {pacienteData.pacienteUserprofileImage ? (
          <Image
            style={styles.image}
            source={{
              uri: pacienteData.imgUri,
            }}
          />
        ) : (
          <Icon name="camera" style={styles.icon} />
        )}
      </TouchableOpacity>
      <Text style={styles.textLabel}>Nombre Completo</Text>
      <TextInput
        defaultValue={props.navigation.getParam('nombreCompleto', '')}
        onChangeText={text => setData({...pacienteData, nombreCompleto: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: Carlos Contreras'}
      />
      <Text style={styles.textLabel}>Fecha de Nacimiento</Text>
      <DatePicker
        style={{width: Dimensions.get('window').width - 30}}
        format={'DD/MM/YYYY'}
        customStyles={{
          dateInput: [styles.textInput, {paddingTop: 20, paddingBottom: 20}],
          dateIcon: {display: 'none'},
        }}
        onDateChange={date =>
          setData({...pacienteData, fechaDeNacimiento: date})
        }
        date={pacienteData.fechaDeNacimiento}
        placeholder={'Ejemplo: 10/10/2010'}
      />
      <Text style={styles.textLabel}>Dirección</Text>
      {/* GOOGLE MAPS */}
      <View>
        <GooglePlacesAutocomplete
          enablePoweredByContainer={false}
          placeholder={'Dirección'}
          fetchDetails={true}
          textInputProps={{
            onFocus: () => setSearchLocation(true),
            onBlur: () => setTimeout(() => setSearchLocation(false), 300),
          }}
          listViewDisplayed={searchLocation}
          renderDescription={row => row.description || row.vicinity}
          onPress={(data, details) => {
            selectPlace({
              destination: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
              },
              title: data.description,
            });
          }}
          styles={{
            textInputContainer: {
              height: 80,
              borderTopWidth: 0,
              backgroundColor: 'transparent',
            },
            textInput: [
              styles.textInput,
              {
                paddingTop: 0,
                paddingBottom: 0,
                paddingRight: 0,
                paddingLeft: 0,
                padding: 0,
                height: 44,
              },
            ],
            listView: {
              borderWidth: 1,
              borderColor: '#DDD',
              backgroundColor: '#FFF',
              elevation: 5,
              shadowColor: '#000',
              shadowOpacity: 0.2,
              shadowOffset: {x: 0, y: 0},
              shadowRadius: 15,
              marginTop: 10,
            },
          }}
          query={{
            key: 'AIzaSyDcCkG9ip0E9qmUuV3M9iCKFC-gkznx9T8',
            language: 'pt',
          }}
        />
      </View>
      <MapView
        showsUserLocation={true}
        style={styles.map}
        region={{
          latitude: localSelected.destination.latitude,
          longitude: localSelected.destination.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.0921,
        }}
        initialRegion={{
          latitude: -34.5875885,
          longitude: -58.4027622,
          latitudeDelta: 0.09,
          longitudeDelta: 0.0921,
        }}>
        <Marker coordinate={localSelected.destination} />
      </MapView>
      <Text style={styles.textLabel}>DNI</Text>
      <TextInput
        defaultValue={props.navigation.getParam('DNI', '')}
        style={styles.textInput}
        onChangeText={text => setData({...pacienteData, DNI: text})}
        placeholder={'Ejemplo: 9999999999'}
      />
      <Text style={styles.textLabel}>Diagnostico</Text>
      <TextInput
        defaultValue={props.navigation.getParam('diagnostico', '')}
        onChangeText={text => setData({...pacienteData, diagnostico: text})}
        style={styles.textInput}
        placeholder={'Ejemplo: depresión aguda y ansiedad'}
      />
      {pacienteData.temPsi ? (
        <View>
          <Text style={styles.textLabel}>Psiquiatra</Text>
          <TextInput
            defaultValue={props.navigation.getParam('psiquiatra', '')}
            onChangeText={text => setData({...pacienteData, psiquiatra: text})}
            style={styles.textInput}
            placeholder={'Informe el nombre del médico.'}
            C
          />
          <Text style={styles.textLabel}>Teléfono del Psiquiatra</Text>
          <TextInput
            defaultValue={props.navigation.getParam('telefonoPsiquiatra', '')}
            onChangeText={text =>
              setData({...pacienteData, telefonoPsiquiatra: text})
            }
            style={styles.textInput}
            placeholder={'Ejemplo: 11 2345 6789'}
          />
          <Text style={styles.textLabel}>Psicologo</Text>
          <TextInput
            defaultValue={props.navigation.getParam('psicologo', '')}
            onChangeText={text => setData({...pacienteData, psicologo: text})}
            style={styles.textInput}
            placeholder={'Ejemplo: Carlos Andrade'}
          />
          <Text style={styles.textLabel}>Teléfono del Psicológo</Text>
          <TextInput
            defaultValue={props.navigation.getParam('telefonoPsicologo', '')}
            onChangeText={text =>
              setData({...pacienteData, telefonoPsicologo: text})
            }
            style={styles.textInput}
            placeholder={'Ejemplo: 11 2345 6789'}
          />
        </View>
      ) : (
        <></>
      )}
      <CheckBox
        onPress={() => {
          alert('works');
        }}
        label={'No tengo prevision.'}
        style={{marginBottom: 10, marginTop: 10}}
        rightText={'No tengo ningún psicologo o psiquiatra.'}
        onClick={() => {
          setData({
            ...pacienteData,
            temPsi: !pacienteData.temPsi,
          });
        }}
        isChecked={!pacienteData.temPsi}
      />
      <TouchableOpacity
        style={[styles.btnNext, loading ? styles.loading : {}]}
        disabled={loading}
        onPress={() => {
          if (validateForm(pacienteData)) {
            let data = pacienteData;
            data = {
              ...data,
              direccion: localSelected.title,
              coordinadasDireccion: {
                type: 'Point',
                coordinates: [
                  localSelected.destination.latitude,
                  localSelected.destination.longitude,
                ],
              },
            };
            delete data.imgUri;
            setLoading(true);
            if (props.userLogged.userType === 'funcionario') {
              createPaciente(
                {
                  ...data,
                  instituicao: props.userLogged.instituicao._id,
                  chat: [],
                },
                props,
                () => {
                  setLoading(false);
                },
              );
            } else if (props.userLogged.userType === 'familiar') {
              createPacienteFamiliar(
                {
                  ...data,
                  familiar: props.userLogged._id,
                  chat: [],
                },
                props,
                () => {
                  setLoading(false);
                },
              );
            }
          } else {
            Alert.alert(
              'Completa todos los campos',
              'Debes completar todos los campos.',
            );
          }
        }}>
        <Text style={styles.uppercaseAndWhite}>
          {loading ? 'Carregando...' : 'Finalizar Registro'}
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  imageContainer: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
  },
  icon: {
    fontSize: 40,
  },
  uppercaseAndWhite: {
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  textInput: {
    padding: 9,
    paddingLeft: 15,
    textAlign: 'center',
    borderRadius: 50,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(98,100,207,1)',
  },
  map: {
    height: 300,
    width: Dimensions.get('window').width - 20,
  },
  loading: {
    color: '#FFF',
    backgroundColor: '#CCC',
  },
  btnNext: {
    alignItems: 'center',
    padding: 17,
    marginTop: 15,
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    backgroundColor: 'rgba(98,100,207,1)',
    borderRadius: 50,
  },
  textLabel: {
    alignSelf: 'center',
    marginTop: 10,
  },
});

const mapStateToProps = state => ({
  userLogged: state.usuarios.userLogged,
});

const mapDispatchToProps = dispatch => ({
  setPaciente: data => dispatch({type: Actions.setPaciente, payload: data}),
  getPacientesSuccess: data =>
    dispatch({type: Actions.getPacientesSuccess, payload: data}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CadastroPaciente);
