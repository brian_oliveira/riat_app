/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from '../../assets/colors';

import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';

const Item = props => {
  return (
    <View style={styles.itemContainer}>
      <Text style={styles.label}>{props.label}</Text>
      <Text style={styles.itemText}>{props.labelValue}</Text>
    </View>
  );
};

// TODO: A Instituição tem uma opção de por de dispositivo!

const PacienteDetalhe = props => {
  const selectFamiliarOrInst = () => {
    switch (props.userLogged.userType) {
      case 'funcionario':
        return props.pacienteSelected;
      case 'familiar':
        return props.pacienteSelectedFamiliar;
      default:
        Alert.alert('Falha!', 'Você está tentando hackear o sistema?');
        props.navigate.goBack();
        return {};
    }
  };

  const [paciente, setPaciente] = useState(selectFamiliarOrInst());

  useEffect(() => {
    setPaciente(selectFamiliarOrInst());
  });

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={{alignItems: 'center'}}>
        <Image
          style={styles.profileImage}
          source={
            'familiar' in paciente
              ? {
                  uri: `https://riat.s3.amazonaws.com/public/${
                    paciente._id.$oid
                  }_paciente_userprofile_image`,
                }
              : {
                  uri:
                    'https://images.unsplash.com/photo-1548365108-908055adb1c8?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
                }
          }
        />
      </View>
      <Item
        label="Nombre Completo:"
        labelValue={paciente.nombreCompleto}
      />
      <Item label="DNI:" labelValue={paciente.DNI} />
      <Item label="Fecha de Nascimento:" labelValue="24/11/1995" />
      <Item
        label="Diagnóstico:"
        labelValue={paciente.diagnostico}
      />
      <Item label="Dirección:" labelValue={paciente.direccion} />
      <Item
        label="Psicologo:"
        labelValue={
          paciente.temPsi
            ? paciente.psicologo
            : 'Aún no tiene psicologo.'
        }
      />
      <Item
        label="Teléfono del Psicologo:"
        labelValue={
          paciente.temPsi
            ? paciente.telefonoPsicologo
            : 'Aún no tiene psicologo.'
        }
      />
      <Item
        label="Psiquiatra:"
        labelValue={
          paciente.temPsi
            ? paciente.psiquiatra
            : 'Aún no tiene psiquiatra.'
        }
      />
      <Item
        label="Teléfono del Psiquiatra:"
        labelValue={
          paciente.temPsi
            ? paciente.telefonoPsiquiatra
            : 'Aún no tiene psiquiatra.'
        }
      />
      <View style={styles.footer}>
        <TouchableOpacity
          style={styles.editButton}
          onPress={() =>
            props.navigation.navigate(
              'CadastroApenasPaciente',
              props.pacienteSelected,
            )
          }>
          <Icon name="edit" size={20} color="#FFF" />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  label: {
    fontSize: 16,
    marginHorizontal: 15,
  },
  itemText: {
    fontSize: 14,
    textAlign: 'left',
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#eee',
    marginVertical: 20,
  },
  editButton: {
    backgroundColor: Colors.colorRed,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.45,
    shadowRadius: 4.65,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
  },
  footer: {
    position: 'absolute',
    right: 15,
    bottom: 15,
    alignItems: 'flex-end',
  },
});

const mapStateToProps = state => ({
    userLogged: state.usuarios.userLogged,
  pacienteSelected: state.instituicao.pacienteSelected,
  pacienteSelectedFamiliar: state.familiar.pacienteSelected,
});

export default connect(mapStateToProps)(PacienteDetalhe);
