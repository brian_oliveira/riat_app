import {URL} from '../assets/consts';
import axios from 'axios';
import {Alert} from 'react-native';

const getPacientes = props => {
  let url = URL + 'familiar/incoming_webhook/getPacientesFromFamiliar';

  axios
    .post(url, {id: props.userLogged._id})
    .then(response => {
      props.getPacientesSuccess(response.data);
    })
    .catch(err => alert('Erro Familiar (Get pacientes):' + err));
};

const createPacienteFamiliar = (data, props, callbackSuccess = () => {}) => {
  axios
    .post(URL + 'paciente/incoming_webhook/createPaciente', data)
    .then(data => {
      Alert.alert('Sucesso!', 'Paciente cadastrado com sucesso!');
      getPacientes(props);
      props.navigation.goBack();
    })
    .catch(err => {
      Alert.alert('Perdón!', 'Aconteció un error... ' + err);
    });
};
export {getPacientes, createPacienteFamiliar};
