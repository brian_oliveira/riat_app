import {URL} from '../assets/consts';
import axios from 'axios';
import {Alert} from 'react-native';
import RNSmtpMailer from 'react-native-smtp-mailer';

const getPacientes = props => {
  let url = URL + 'instituicao/incoming_webhook/getPacientes';
  axios
    .post(url, {instituicaoId: props.userLogged.instituicao._id})
    .then(result => {
      props.setPacientes(result.data);
    })
    .catch(err => alert('' + err));
};

const getSessoes = props => {
  let url = URL + 'instituicao/incoming_webhook/getSessoesFromInstituicao';

  axios
    .post(url, {instituicaoId: props.userLogged.instituicao._id})
    .then(result => {
      props.setSessoesInstituicao(result.data);
    })
    .catch(err => alert('' + err));
};

const createPaciente = (data, props, callbackSuccess = () => {}) => {
  axios
    .post(URL + 'instituicao/incoming_webhook/createPaciente', data)
    .then(data => {
      Alert.alert('Sucesso!', 'Paciente cadastrado com sucesso!');
      getPacientes(props);
      callbackSuccess();
      props.navigation.goBack();
    })
    .catch(err => {
      Alert.alert('Perdón!', 'Aconteció un error... ' + err);
    });
};

const sendMail = (email, to, from, idMedico) => {
  RNSmtpMailer.sendMail({
    mailhost: 'email-ssl.com.br',
    port: '465',
    ssl: true,
    username: 'noreply@riat.com.ar',
    password: 'Riat_2019',
    from: 'noreply@riat.com.ar',
    recipients: email,
    subject: 'Você recebeu um convite da Instituição ' + from + '!',
    htmlBody: `<div></div><h1>Olá, ${to}</h1>
                      <p>Você recebeu um convite para acessar o novo sistema
                      da RIAT a partir da instituição que você trabalha, para continuar
                      seu registro <a href="riat-sfhra.mongodbstitch.com/#/newMedico/?id=${idMedico}">clique aqui</a></p>`,
    attachmentPaths: [],
    attachmentNames: [],
    attachmentTypes: [],
  }).then(() => {
    Alert.alert('Sucesso!', 'E-mail enviado com sucesso!');
  });
};

const newMedico = (data, props, callbackSucess = () => {}) => {
  axios
    .post(URL + 'instituicao/incoming_webhook/createPsi', data)
    .then(res => {
      Alert.alert('Muy bien!', 'Médico cadastrado!');
      sendMail(data.email, data.nombre, 'RIAT', res.data.insertedId.$oid);
      callbackSucess();
      props.navigation.goBack();
    })
    .catch(err => {
      Alert.alert('Perdón!', 'Error al adicionar nuevo Médico! ' + err);
    });
};

const setMedicos = props => {
  let url = URL + 'instituicao/incoming_webhook/getMedicos';
  axios
    .post(url, {instituicaoId: props.userLogged.instituicao._id})
    .then(result => {
      props.setMedicos(result.data);
    })
    .catch(err => alert('set medicos: ' + err));
};

export {getPacientes, createPaciente, getSessoes, newMedico, setMedicos};
