import axios from 'axios';
import {URL} from '../assets/consts';

/*const sendMessage = data => {
  axios
    .post(URL + 'paciente/incoming_webhook/addMessage', data)
    .then(res => {
      return true;
      //alert('message sent!');
    })
    .catch(err => {
      alert('Erro ao enviar mensagem: Erro: ' + err);
      return false;
    });
};*/

const sendMessage = async data => {
  try {
    const res = await axios.post(
      URL + 'paciente/incoming_webhook/addMessage',
      data,
    );
    return true;
    //alert('message sent!');
  } catch (err) {
    alert('Erro ao enviar mensagem: Erro: ' + err);
    return false;
  }
};

const updateSolicitacao = data => {
  axios
    .post(URL + 'solicitacao/incoming_webhook/updateSolicitacao', data)
    .then(res => {
      alert('Solicitación atualizada! Gracias por su esfuerzo!');
    })
    .catch(err => {
      alert('Erro ao atualizar solicitação. Erro: ' + err);
    });
};

const getSessoesFromPaciente = (pacienteId, callbackSuccess = () => {}, callbackError = () => {}) => {
  axios
    .post(URL + 'sessoes/incoming_webhook/getSessoesFromPaciente', {
      id: pacienteId,
    })
    .then(res => { callbackSuccess(res) })
    .catch(err => { callbackError(err) });
};

export {sendMessage, updateSolicitacao, getSessoesFromPaciente};
