import axios from 'axios';
import {URL} from '../assets/consts';

class Usuarios {
  static updateFromServer = (id, email, callback) => {
    axios
      .post(URL + 'usuarios/incoming_webhook/loginUserCheck', {
        userId: id,
        email: email,
      })
      .then(callback)
      .catch(err => alert(err));
  };
}

export default Usuarios;
