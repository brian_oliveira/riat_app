import axios from 'axios';
import {optionsS3, URL} from '../assets/consts';
import {RNS3} from 'react-native-s3-upload/src/RNS3';
import {Alert} from 'react-native';

const acceptOrRejectProposta = (data, callbackSuccess = () => {}) => {
  axios
    .post(URL + 'acompanhante/incoming_webhook/acceptRejectProposta', data)
    .then(res => {
      if (data.accepted) {
        alert('Proposta Aceita!');
      } else {
        alert('Proposta rehuasada!');
      }
      callbackSuccess();
    })
    .catch(err => {
      alert(JSON.stringify(data));
      alert('Erro ao enviar mensagem: Erro: ' + err);
    });
};

const checkInToServer = data => {
  axios
    .post(URL + 'solicitacao/incoming_webhook/checkIn', data)
    .then(res => {
      console.log(res.data);
    })
    .catch(err => {
      alert('Erro: ' + err);
    });
};

const checkOutToServer = (data, callbackSuccess) => {
  axios
    .post(URL + 'solicitacao/incoming_webhook/checkout', data)
    .then(res => {
      console.log(res.data);
      callbackSuccess(res);
    })
    .catch(err => {
      alert('Erro: ' + err);
    });
};

const setPacientesToRedux = props => {
  let pacientes = [];
  props.userLogged.solicitacoesAceitas.forEach(solicitacao => {
    pacientes.push(solicitacao.solicitacao.selectedPaciente);
  });
  return pacientes;
};

const prepareRequestsToSendToServer = (requests, index) => {
  let newRequests = [...requests];

  newRequests.forEach((request, index) => {
    newRequests[index] = {
      ...request,
      solicitacao: request.solicitacao._id,
    };
  });

  newRequests.splice(index, 1);

  return newRequests;
};

const sendInformeToServer = (data, callbackSuccess = () => {}) => {
  axios
    .post(URL + 'solicitacao/incoming_webhook/relatorioSessao', data)
    .then(res => {
      Alert.alert('Felicitaciones', 'Su informe ha sido registrado!');
      callbackSuccess();
    })
    .catch(err => alert('Erro ao enviar informe: ' + err));
};

const getSessoesFromAcompanhante = (
  acompanhanteId,
  callbackSuccess = () => {},
) => {
  axios
    .post(
      'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/riat-sfhra/service/sessoes/incoming_webhook/getSessoesFromAcompanhante',
      {id: acompanhanteId},
    )
    .then(res => {
      callbackSuccess(res);
    })
    .catch(err => {
      alert('Error al buscar las sesiones.');
    });
};

const sendDocumentToAmazon = async (userEmail, docType, uri) => {
  let url = '';

  let file = {
    uri: uri,
    name: userEmail + '_' + docType + '.jpg',
    type: 'image/jpg',
  };

  const response = await RNS3.put(file, optionsS3);
  if (response.status !== 201) {
    alert(
      'Problema de conexión con la base de datos. Por favor, intente más tarde.',
    );
  } else {
    return response.body.postResponse.location;
  }
};

export {
  acceptOrRejectProposta,
  prepareRequestsToSendToServer,
  checkInToServer,
  sendInformeToServer,
  checkOutToServer,
  setPacientesToRedux,
  sendDocumentToAmazon,
  getSessoesFromAcompanhante,
};
