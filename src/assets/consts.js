import {Picker} from 'react-native';

const URL =
  'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/riat-sfhra/service/';

const DIAS_SEMANA = [
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado',
  'Domingo',
];

const MESES = [
  'ENE',
  'FEB',
  'MAR',
  'ABR',
  'MAY',
  'JUN',
  'JUL',
  'AGO',
  'SEP',
  'OCT',
  'NOV',
  'DIC',
];

const MESES_EXTENSO = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Semptiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];

const IVA = [
  'Responsable Inscripto',
  'Responsable No Inscripto',
  'Consumidor Final',
  'Sujeto Exento',
  'Responsable Monotributo',
  'Sujeto no Categorizado',
  'Provedor del Exterior',
  'Cliente del Exterior',
  'IVA Liberado',
  'IVA Responsable Inscripto',
  'Pequeño Contribuinte Oficial',
  'Monotributista Social',
  'Pequeño Contribuinte Eventual Social',
];

const FORMA_PAGO = [
  'Tarjeta de Debito',
  'Tarjeta de Credito',
  'Cuenta Corriente',
  'Cheque',
  'Mercado Pago',
];

const AREAS_PROMOVER = [
  'Motivar la adherencia al Proyecto Terapeutico',
  'Fomentar Inserción en las Actividades',
  'Ofrecerse como Modelo Identificador',
  'Oficiar de Guía',
  'Acompañar el proceso de resocialización',
  'Aplicar estrategias de contención',
  'Intevenir en la trama familiar',
  'Ampliar el mundo objetivo del paciente',
  'Ayudar la construcción del contexto cotidiano',
  'Fomentar un espacio para el pensamiento',
  'Elevar la autoestima',
  'Aumentar respeto de las propias decisiones',
  'Promover Actitudes Positivas',
  'Desarrollar la capacidad creativa',
  'Mantener rutinas diarias, auto cuidado y autonomia',
  'Facilitar la conexión con el contexto familiar',
  'Ayudar a la creación de un sentimento de pertenencia',
  'Fortalecer los vinculos',
  'Promocionar la orientación en el espacio social',
  'Concientizar sobre la accesibilidad del ambiente',
  'Ayudar en la recuperación de sus vículos',
];

const IMAGE_PICKER_BASE_CONFIG = {
  title: 'Selecciona una imagen',
  takePhotoButtonTitle: 'Saca una una foto',
  chooseFromLibraryButtonTitle: 'Agrega foto de tu galería',
  storageOptions: {
    skipBackup: true,
    path: 'riat_images',
  },
  quality: 0.3,
  maxWidth: 1200,
  maxHeight: 1200,
};

const TIPO_PROFISSIONAL = [
  'Acompañante Terapéutico',
  'Cuidador',
  'Operador Socio-Terapéutico',
  'Trabajador Social',
  'Psicanalista',
  'Psicólogo',
  'Psiquiatra',
  'Neurólogo',
];

const optionsS3 = {
  keyPrefix: 'uploads/',
  bucket: 'riat',
  region: 'us-east-1',
  accessKey: 'AKIAS2DJ6NXJ56AB4KEG',
  secretKey: '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',
  successActionStatus: 201,
};

export {
  URL,
  IVA,
  MESES,
  FORMA_PAGO,
  AREAS_PROMOVER,
  IMAGE_PICKER_BASE_CONFIG,
  DIAS_SEMANA,
  TIPO_PROFISSIONAL,
  MESES_EXTENSO,
  optionsS3,
};
