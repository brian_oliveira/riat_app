import moment from "moment/min/moment-with-locales";

const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

const getAge = date => {
  let x = new Date().getFullYear() - date.getFullYear();
  if (Number.isNaN(x)) {
    return 'Idade não definida.';
  } else {
    return x;
  }
};

const getMinutesDifference = (date1, date2) => {
  let diffMs = date1 - date2;
  return Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
};

const getHourMinutesStringDifference = (date1, date2) => {
  let minutes = getMinutesDifference(date1, date2);
  let hours = Math.round(minutes / 60);

  return pad(hours, 2) + ':' + pad(minutes - hours * 60, 2);
};

const checkIfDateIsToday = date => {
  let today = new Date();

  if (
    date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
  ) {
    return true;
  }
  return false;
};

const calcularHorasTrabalhadas = (horaInicio, horaFim) => {
  const dateInicio = new Date(horaInicio);
  const dateFim = new Date(horaFim);

};

const returnHorasTrabalhadas = (horaInicio, horaFim) => {
  const dateInicio = new Date(horaInicio);
  const dateFim = new Date(horaFim);

  const diffMs = dateFim - dateInicio;
  const diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
  let hours = diffMins / 60;
  return Math.floor(hours);
};

const getValorTotal = (sessoes, valorHora) => {
  let array = [];
  const reducer = (accumulator, currentValue) => accumulator + currentValue;

  sessoes.forEach(sessao => {
    array.push(
      returnHorasTrabalhadas(sessao.horaInicio, sessao.horarioFim) *
        parseFloat(valorHora),
    );
  });
  return array.reduce(reducer, 0);
};

export {
  pad,
  calcularHorasTrabalhadas,
  getValorTotal,
  returnHorasTrabalhadas,
  getMinutesDifference,
  getHourMinutesStringDifference,
  checkIfDateIsToday,
  getAge,
};
