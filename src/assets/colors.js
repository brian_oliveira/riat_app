const Colors = {
  colorBlue: '#2E5894',
  colorRed: '#CA0052',
  colorLightBlue: '#6E91B5',
  colorGreen: '#548D39',
};

export default Colors;
