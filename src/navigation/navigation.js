import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginPage from '../screens/login';
import InstituicaoIndex from '../screens/instituicao';
import PacienteListagem from '../screens/pacientes/pacientes';
import PacienteDetalhe from '../screens/pacientes/paciente_detalhe';
import ListaPacientesPsi from '../screens/psi/menuOptions/pacientes/lista_pacientes';
import CadastroApenasPaciente from '../screens/pacientes/cadastro_paciente';
import MedicoListagem from '../screens/instituicao/menuOptions/equipoMedico/equipo_medico';
import MedicoDetalhe from '../screens/instituicao/menuOptions/equipoMedico/medico_detalhe';
import AdicionarMedico from '../screens/instituicao/menuOptions/equipoMedico/adicionar_medico';
import AcompanamientosInstituicao from '../screens/instituicao/menuOptions/acompanamientos/acompanamientos';
import AcompanamientosDetalhe from '../screens/instituicao/menuOptions/acompanamientos/acompanamientos_detalhe';
import HistoriaClinicaPacientes from '../screens/psi/menuOptions/historiaClinica/lista_pacientes';
import AdministrativoInstituicao from '../screens/instituicao/menuOptions/administrativo/administrativo';
import AdministrativoPaciente from '../screens/instituicao/menuOptions/administrativo/administrativo_paciente';
import InformesPaciente from '../screens/instituicao/menuOptions/informes_paciente/informes_paciente';
import ProfissionalMenu from '../screens/profissional';
import FamiliarMenu from '../screens/familiar';
import TipoProfissional from '../screens/profissional/cadastro/tipoProfissional';
import CadastroGeral from '../screens/login/cadastroGeral';
import CadastroPaciente from '../screens/familiar/cadastroPaciente';
import CadastroInstituicao from '../screens/instituicao/cadastro';
import VisitanteIndex from '../screens/visitante';
import PacienteDetalhePsi from '../screens/psi/menuOptions/pacientes/paciente_detalhe'
import Experiencia from '../screens/profissional/cadastro/experiencia';
import PreferenciasCadastroProfissional from '../screens/profissional/cadastro/preferencias';
import DocumentosCadastroProfissional from '../screens/profissional/cadastro/documentos';
import Administrativo from '../screens/profissional/menuOptions/administrativo/administrativo';
import AgendaDeTrabajo from '../screens/profissional/menuOptions/agenda_de_trabajo/agenda_de_trabajo';
import HistoricoDeTrabajo from '../screens/profissional/menuOptions/historico/historico_de_trabajo';
import InformesMenu from '../screens/profissional/menuOptions/informes/informes_menu';
import PsiMenu from '../screens/psi/index';
import AreaDiagnosticada from '../screens/psi/menuOptions/historiaClinica/area_diagnosticada';
import Informe from '../screens/profissional/menuOptions/informes/informe';
import Rooms from '../screens/chat/rooms';
import Chat from '../screens/chat/chat';
import Acompanamientos from '../screens/profissional/menuOptions/acompanamientos/acompanamientos';
import RecibirSolicitud from '../screens/profissional/menuOptions/acompanamientos/recibir_solicitud_local';
import RecibirSolicitudDiagnostico from '../screens/profissional/menuOptions/acompanamientos/recibir_solicitud_diagnostico';
import SolicitudAcompanamiento from '../screens/familiar/menuOptions/solicitud_acompanamiento';
import AdministrativoFamiliar from '../screens/familiar/menuOptions/administrativo';
import Noticias from '../screens/visitante/menuOptions/noticias';
import NoticiaPage from '../screens/visitante/menuOptions/noticia_page';
import PacienteInforme from '../screens/psi/menuOptions/pacientes/paciente_informe';
import QuienesSomos from '../screens/visitante/menuOptions/quienes_somos';
import PacienteInformes from '../screens/psi/menuOptions/pacientes/paciente_informes';

const navigator = createStackNavigator({
  //Home
  Home: {
    screen: LoginPage,
    navigationOptions: {
      header: null,
    },
  },
  //Menus
  InstituicaoMenu: {
    screen: InstituicaoIndex,
    navigationOptions: {
      title: 'Menu',
    },
  },
  ProfissionalMenu: {
    screen: ProfissionalMenu,
    navigationOptions: {
      title: 'Menu',
    },
  },
  FamiliarMenu: {
    screen: FamiliarMenu,
    navigationOptions: {
      title: 'Menu',
    },
  },
  VisitanteMenu: {
    screen: VisitanteIndex,
    navigationOptions: {
      title: 'Menu',
    },
  },
  //MEDICO PSI
  PsiMenu: {
    screen: PsiMenu,
    navigationOptions: {
      title: 'Menu',
    },
  },
  HistoriaClinica: {
    screen: HistoriaClinicaPacientes,
    navigationOptions: {
      title: 'Historia Clínica',
    },
  },
  AreaDiagnosticada: {
    screen: AreaDiagnosticada,
    navigationOptions: {
      title: 'Area Diagnosticada',
    },
  },
  ListaPacientesPsi: {
    screen: ListaPacientesPsi,
    navigationOptions: {
      title: 'Pacientes',
    },
  },
  PacienteDetalhePsi: {
    screen: PacienteDetalhePsi,
    navigationOptions: {
      title: 'Detalhes do Paciente',
    },
  },
  PacienteInformes: {
    screen: PacienteInformes,
    navigationOptions: {
      title: 'Informes',
    },
  },
  PacienteInforme: {
    screen: PacienteInforme,
    navigationOptions: {
      title: 'Informe de la Sesión',
    },
  },
  //Cadastros
  CadastroGeral: {
    screen: CadastroGeral,
    navigationOptions: {
      title: 'Cadastro General',
    },
  },
  CadastroInstituicao: {
    screen: CadastroInstituicao,
    navigationOptions: {
      title: 'Cadastro de Instituición',
    },
  },
  AdicionarMedico: {
    screen: AdicionarMedico,
    navigationOptions: {
      title: 'Adicionar Médico',
    },
  },
  CadastroApenasPaciente: {
    screen: CadastroApenasPaciente,
    navigationOptions: {
      title: 'Cadastro Paciente',
    },
  },
  CadastroPaciente: {
    screen: CadastroPaciente,
    navigationOptions: {
      title: 'Cadastro de Paciente',
    },
  },
  TipoProfesional: {
    screen: TipoProfissional,
    navigationOptions: {
      title: 'Tipo de Profesional',
    },
  },
  ExperienciaProfesional: {
    screen: Experiencia,
    navigationOptions: {
      title: 'Experiencia Profesional',
    },
  },
  PreferenciasProfesional: {
    screen: PreferenciasCadastroProfissional,
    navigationOptions: {
      title: 'Preferencias del Profesional',
    },
  },
  DocumentosProfesional: {
    screen: DocumentosCadastroProfissional,
    navigationOptions: {
      title: 'Documentos Profisionales',
    },
  },
  //================== CHAT =================
  ChatRooms: {
    screen: Rooms,
    navigationOptions: {
      title: 'Chat',
    },
  },
  Chat: {
    screen: Chat,
  },
  // ================= MENU =================
  // PROFISSIONAL
  Administrativo: {
    screen: Administrativo,
    navigationOptions: {
      title: 'Administrativo',
    },
  },
  AgendaDeTrabajo: {
    screen: AgendaDeTrabajo,
    navigationOptions: {
      title: 'Agenda de Trabalho',
    },
  },
  HistoricoDeTrabajo: {
    screen: HistoricoDeTrabajo,
    navigationOptions: {
      title: 'Histórico de Trabajo',
    },
  },
  InformesMenu: {
    screen: InformesMenu,
    navigationOptions: {
      title: 'Menu de Informes',
    },
  },
  Informe: {
    screen: Informe,
    navigationOptions: {
      title: 'Informe',
    },
  },
  // FAMILIAR
  SolicitacaoAcompanhamento: {
    screen: SolicitudAcompanamiento,
    navigationOptions: {
      title: 'Nueva Solicitud',
    },
  },
  AdministrativoFamiliar: {
    screen: AdministrativoFamiliar,
    navigationOptions: {
      title: 'Administrativo Familiar',
    },
  },
  // INSTITUICAO
  PacienteListagem: {
    screen: PacienteListagem,
    navigationOptions: {
      title: 'Pacientes',
    },
  },

  PacienteDetalhe: {
    screen: PacienteDetalhe,
    navigationOptions: {
      title: 'Datos del Paciente',
    },
  },
  MedicoListagem: {
    screen: MedicoListagem,
    navigationOptions: {
      title: 'Equipo Medico',
    },
  },
  MedicoDetalhe: {
    screen: MedicoDetalhe,
    navigationOptions: {
      title: '',
    },
  },
  AcompanamientosInstituicao: {
    screen: AcompanamientosInstituicao,
    navigationOptions: {
      title: 'Servicios',
    },
  },
  AcompanamientosDetalhe: {
    screen: AcompanamientosDetalhe,
    navigationOptions: {
      title: 'Detalles del Servicio',
    },
  },
  AdministrativoInstituicao: {
    screen: AdministrativoInstituicao,
    navigationOptions: {
      title: 'Administrativo',
    },
  },
  AdministrativoPaciente: {
    screen: AdministrativoPaciente,
    navigationOptions: {
      title: '',
    },
  },
  InformesPaciente: {
    screen: InformesPaciente,
    navigationOptions: {
      title: 'Informes por Acompanhamento',
    },
  },

  Acompanamientos: {
    screen: Acompanamientos,
    navigationOptions: {
      title: 'Acompañamientos',
    },
  },
  RecibirSolicitud: {
    screen: RecibirSolicitud,
    navigationOptions: {
      title: 'Localización del Paciente',
    },
  },
  RecibirSolicitudDiagnostico: {
    screen: RecibirSolicitudDiagnostico,
    navigationOptions: {
      title: 'Diagnóstico del Paciente',
    },
  },
  //VISITANTE
  Noticias: {
    screen: Noticias,
    navigationOptions: {
      title: 'Noticias',
    },
  },
  NoticiaPage: {
    screen: NoticiaPage,
    navigationOptions: {
      title: '',
    },
  },
  QuemSomos: {
    screen: QuienesSomos,
    navigationOptions: {
      title: 'Quienes Somos',
    },
  },
});

const Navigator = createAppContainer(navigator);

export default Navigator;
