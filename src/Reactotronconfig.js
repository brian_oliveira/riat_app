/* eslint-disable prettier/prettier */
import Reactotron from 'reactotron-react-native';

if (__DEV__) {
  // colocar o endereco ip atual
  const tron = Reactotron.configure({ host: '192.168.1.5' })
    .useReactNative()
    .connect();
  tron.clear();
  console.tron = tron;
}
